<?php
if (!defined('ABSPATH')) exit;

/**
 * The main template file
 *
 * @package Hotwire
 */

$image = get_field('404_image', 'option');
$imageInitialUrl = '';
$imageFullUrl = '';

if (is_array($image) && !empty($image)) {
	$imageInitialUrl = $image['sizes']['initial'];
	$imageFullUrl = $image['sizes']['large'];
}

?>
<?php get_header(); ?>
<?php
	echo Hotwire_Helper::getTemplatePart('template-parts/page-header', [
		'imageInitialUrl' => $imageInitialUrl,
		'imageFullUrl' => $imageFullUrl,
		'type' => Hotwire_ACF_Page_Header::TYPE_MEDIUM_ACCENT,
		'title' => 'Oops!',
		'subtitle' => 'Page not<br/>found',
	]);
?>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/index'); ?>
<?php get_footer(); ?>
