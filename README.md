# README #

This repo contains the theme for the new Hotwire website build.

### What is this repository for? ###

* Developers

### How do I get set up? ###

* Get Wordpress up and running on your local machine
* Clone the repo into `/wp-content/themes/`
* Activate the theme in the Wordpress admin panel

### Development ###

* The assets for this theme are built using VueJS + Webpack which can be found under the `/dev` directory
* The `/assets` directory gets built each time Webpack is run, so all static assets must be located under `/dev/static`

### Content Import ###

* Existing blog posts can be imported
* This relies on constants being defined (normally in `wp-config.php`)
* It is recommended that this script only be run locally and not in production
* View `/inc/hotwire.php` for further details

### Deployment ###

* Ensure you update the version number in `package.json` and `style.css`. This force the re-loading of assets once deployed.
* Assets are built for production by running `npm run build` from within the `/dev` directory.
* Deployment is achieved by running a simply bash script found in `~/hotwire-deploy`. You'll need to ssh into the server in order to run this. This script simply:
 * Creates a .tar.gz backup of the live theme directory
 * Clones the latest repo branch
 * Removes the cloned dev directory
 * Replaces the live theme folder entirely

### Who do I talk to? ###

* Repo owner or admin
