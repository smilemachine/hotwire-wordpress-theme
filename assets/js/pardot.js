$(function() {
  $('.pardot-label-as-placeholder input[type="text"').each(function() {
    updateFormInputStatus($(this));
  });

  $('.pardot-label-as-placeholder input[type="text"').on('change paste keyup', function() {
    updateFormInputStatus($(this));
  });

  function updateFormInputStatus(element) {
    if (element.val().length == 0) {
      element.parent('.form-field').addClass('has-input');
    } else {
      element.parent('.form-field').removeClass('has-input');
    }
  }
});
