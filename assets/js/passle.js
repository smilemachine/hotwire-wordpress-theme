var $ = jQuery;

if ($('#passle-plugin-container').length == 1) {
  let limit = Number($('#passle-plugin-container').attr('data-limit'));
  let filter = $('#passle-plugin-container').attr('data-filter');
  let readMoreText = $('#passle-plugin-container').attr('data-readmore');
  let loadMoreText = $('#passle-plugin-container').attr('data-loadmore');
  let userId = '102e597';

  $('#passle-plugin-container').passle({
    userId: userId,
    numberOfPosts: limit,
    layout: 'none',
    filter: filter
  }, function () {
    passleAppendReadMore();
    passleAppendLoadMore();

    let count = $('#passle-plugin-container .passle-plugin-content .passle-post-container').length;
    $('#passle-plugin-container').addClass('x' + count);

    $('#passle-plugin-container').on('click', '.load-more', function () {
      $(this).html('<span class="fa fa-cog fa-spin"></span>');
      passleLoadMore();
    });
  });

  /**
   * Append read-more buttons to the posts
   *
   * @return void
   */
  function passleAppendReadMore() {
    $('#passle-plugin-container .passle-plugin-content .passle-post-container').each(function () {

      if ($(this).find('.btn-container').length == 0) {
        $(this).append('<div class="btn-container"><span class="btn btn-primary">' + readMoreText + '</span></div>');
      }
    });
  }

  /**
   * Remove load-more from the feed
   *
   * @return void
   */
  function passleRemoveLoadMore() {
    $('#passle-plugin-container .passle-plugin-content .load-more').remove();
  }

  /**
   * Append load-more to the feed
   *
   * @return void
   */
  function passleAppendLoadMore() {
    let loadMoreText = $('#passle-plugin-container').attr('data-loadmore');

    passleRemoveLoadMore();

    if ($('#passle-plugin-container .passle-plugin-content .load-more').length == 0
      && loadMoreText !== undefined && loadMoreText !== false && loadMoreText != '') {
      let html = '<span class="load-more btn btn-primary">' + loadMoreText + '</span>';
      $('#passle-plugin-container .passle-plugin-content').append(html);
    }
  }

  /**
   * Passle's load-more seems to only work with layouts that are not 'none'... damnit
   *
   * @return void
   */
  function passleLoadMore() {
    let page = Number($('#passle-plugin-container .passle-plugin-content').attr('data-page')) + 1;
    let filter = $('#passle-plugin-container .passle-plugin-content').attr('data-filter');
    let url = 'http://clientapi.passle.net/api/PassleContent/' +
      userId + '/' + limit + '/' + page + '/' + filter + '/' + '?layout=none';

    $.get(url, function (response) {
      $('#passle-plugin-container .passle-plugin-content').attr('data-page', page);
      $('#passle-plugin-container').append('<div class="temporary hide">' + response + '</div>');

      let newHtml = $('#passle-plugin-container .hide .passle-plugin-content').html();

      $('#passle-plugin-container .temporary').remove();
      $('#passle-plugin-container .passle-plugin-content').append(newHtml);

      passleAppendReadMore();

      if (newHtml.indexOf('last-page') !== -1) {
        passleRemoveLoadMore();
      } else {
        passleAppendLoadMore();
      }
    });
  }
}

$('#passle-plugin-container').on('click', '.passle-post-container', function (e) {
  e.preventDefault();
  let redirectWindow = window.open($(this).find('.passle-post-title a').attr('href'), '_blank');
  redirectWindow.location;
});
