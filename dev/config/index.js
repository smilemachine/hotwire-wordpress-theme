// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')

module.exports = {
  build: {
    env: require('./prod.env'),
    assetsRoot: path.resolve(__dirname, './../../assets'),
    assetsSubDirectory: '',
    assetsPublicPath: '/wp-content/themes/hotwire-wordpress-theme/assets',
    sourceMap: true,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    gzip: false,
    gzipExtensions: ['js', 'css'],
    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  },
  dev: {
    env: require('./dev.env'),
    assetsRoot: path.resolve(__dirname, './../../assets'),
    assetsSubDirectory: '',
    assetsPublicPath: '/wp-content/themes/hotwire-wordpress-theme/assets',
    sourceMap: false,
    gzip: false,
    gzipExtensions: [],
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
