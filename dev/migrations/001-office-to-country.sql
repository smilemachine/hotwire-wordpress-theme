/**
 * We're switching offices to countries, so we need to update the post type of
 * hw_office to hw_country. This applies to wp_posts as well as wp_postmeta.
 */
UPDATE `wp_posts` SET `post_type` = "hw_country" WHERE `post_type` = "hw_office";

UPDATE `wp_postmeta` SET `meta_key` = "country" WHERE `meta_key` = "office";
UPDATE `wp_postmeta` SET `meta_key` = "_country" WHERE `meta_key` = "_office";

UPDATE `wp_postmeta` SET `meta_key` = "country_heading" WHERE `meta_key` = "office_heading";
UPDATE `wp_postmeta` SET `meta_key` = "_country_heading" WHERE `meta_key` = "_office_heading";

UPDATE `wp_posts` SET `guid` = REPLACE(`guid`, "http://http://", "http://") WHERE `guid` LIKE "http://http://%";
UPDATE `wp_posts` SET `guid` = REPLACE(`guid`, "hw_office", "hw_country") WHERE `guid` LIKE "%hw_office%";

UPDATE `wp_options` SET `option_value` = REPLACE(`option_value`, 's:9:"hw_office"', 's:10:"hw_country"') WHERE `option_name` = "polylang";
