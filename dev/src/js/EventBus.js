import Vue from 'vue'

let EventBus = new Vue({
  methods: {
    listen (app) {
      this.$on('launchVideoFullscreen', function (url) {
        app.launchVideoFullscreen(url)
      })

      this.$on('closeVideoFullscreen', function () {
        app.closeVideoFullscreen()
      })

      this.$on('caseStudyHasLoaded', function() {
        app.caseStudyHasLoaded()
      })
    }
  }
})

export default EventBus
