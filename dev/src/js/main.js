import Vue from 'vue'
import App from './App'
import * as VueGoogleMaps from 'vue2-google-maps'
import BrandsAnimatedBackground from './components/BrandsAnimatedBackground'
import BrandsCarousel from './components/BrandsCarousel'
import EventBus from './EventBus'
import ImagesLoaded from 'imagesloaded'
import InstagramFeed from './components/InstagramFeed'
import jQuery from 'jquery'
import Masonry from 'masonry-layout'
import mixitup from 'mixitup'
import SlickImage from './components/SlickImage'
import Tweet from 'vue-tweet-embed'
import VerticalCarousel from './components/VerticalCarousel'
import VideoFullscreen from './components/VideoFullscreen'
import VueResource from 'vue-resource'

// ToDo: Add helpers to clean up this file

Vue.config.productionTip = false
Vue.config.devTools = (process.env.NODE_ENV != 'production');

Vue.component('app', App)
Vue.component('brands-animated-background', BrandsAnimatedBackground)
Vue.component('brands-carousel', BrandsCarousel)
Vue.component('instagram-feed', InstagramFeed)
Vue.component('slick-image', SlickImage)
Vue.component('tweet', Tweet)
Vue.component('vertical-carousel', VerticalCarousel)
Vue.component('video-fullscreen', VideoFullscreen)

Vue.use(VueResource)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB6lVECTJKwWMLOmj10etMWf55tG0B0Bdg'
  }
})

require('bootstrap')

window.EventBus = EventBus

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data () {
    return {
      elements: {
        caseStudy: {
          loadedViaAjax: false
        },
        map: {
          options: {
            styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
          }
        },
        videoFullscreen: {
          visible: false,
          url: null
        }
      },
      menuClasses: {
        closed: true,
        closing: false,
        open: false,
        opening: false
      },
      scrollDir: 'down',
      scrollTop: 0
    }
  },
  mounted () {
    window.EventBus.listen(this)

    this.setupHeaderHeight()
    this.setupHeaderScrollInteraction()
    this.setupLeadershipTeamOverlay()
    this.setupMasonryLayout()
    this.setupWorkFilters()

    /**
     * If the stored hw.currentUrl = this url without the hash it means
     * that the user has come from the case study and is trying to go
     * 'back' to work. If so, redirect to work. Bit nasty really.
     *
     * Otherwise, mark the page mounted and continue as normal.
     */
    if (window.location.href.includes('#') === true) {
      let previousUrl = localStorage.getItem('hw.currentUrl')
      let nonHashedUrl = window.location.href.replace('#', '')

      if (!_.isNull(previousUrl) && previousUrl == nonHashedUrl + '/') {
        let urlSegments = previousUrl.split('/')

        urlSegments.pop() // trailing slash
        urlSegments.pop() // case-study-url
        window.location = urlSegments.join('/')
      } else {
        window.location = nonHashedUrl
      }
    } else {
      jQuery('body').addClass('mounted')
      this.storeUrlInLocalStorage()
    }
  },
  methods: {
    caseStudyHasLoaded () {
      this.elements.caseStudy.loadedViaAjax = true
    },
    closeMenu () {
      this.menuClasses.closed = false
      this.menuClasses.closing = true
      this.menuClasses.open = false

      jQuery('body').removeClass('menu-open')

      setTimeout(function () {
        this.menuClasses.closed = true
        this.menuClasses.closing = false
      }.bind(this), 1000)
    },
    closeVideoFullscreen () {
      this.elements.videoFullscreen.visible = false
    },
    launchCaseStudy (id, event) {
      var parent = jQuery(event.currentTarget).parent('.work-item')
      var pageHeader = parent.find('.page-header')
      var height = parent.outerHeight()
      var top = parent.offset().top - jQuery(document).scrollTop()
      var url = jQuery(event.currentTarget).attr('href')
      var html = ''

      this.$http.get(url, { params: { ajax: true } })
        .then(response => {
          html = response.body
        }, response => {
          window.location = url
        })

      // Prepare for launch
      var launchStepOne = function () {
        return new Promise(function (resolve, reject) {
          jQuery('body').addClass('launch-case-study')
          parent.addClass('launch')
          resolve(true)
        })
      }

      // Fix the selected article in-place on the page
      var launchStepTwo = function () {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            pageHeader.addClass('launch-step-2')
            pageHeader.css({
              top: top,
              minHeight: height + 'px'
            })

            // Remove un-necessary items
            parent.find('a:not(.back)').remove()
            jQuery('#content > .page-header').remove()
            jQuery('#content > .work-filter').remove()
            jQuery('#content > .work-content .work-item:not(.launch)').remove()

            resolve(true)
          }, 750)
        })
      }

      // Move body and page header to the top, reset the page header height
      var launchStepThree = function () {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            jQuery('html, body').animate({ scrollTop: 0 }, 150)
            pageHeader.addClass('launch-step-3')
            pageHeader.css({
              top: '',
              minHeight: ''
            })
            resolve(true)
          }, 0)
        })
      }

      // Show the titles
      var launchStepFour = function () {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            pageHeader.addClass('launch-step-4')
            resolve(true)
          }, 300)
        })
      }

      // Prepare for new content
      var launchStepFive = function () {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            if (html === '') {
              window.location = url
            } else {
              jQuery('body').addClass('single-hw_casestudy')
              jQuery('#content > *:not(.work-content)').remove()
              jQuery('#content').append('<div id="ajax-case-study">' + html + '</div>')

              /**
               * Bit of a hack, but there's no point implementing
               * an entire version of a CaseStudy + DynamicContent
               * in Vue just for this transition feature
               */
              new Vue({
                el: '#ajax-case-study',
                data () {
                  return {}
                }
              })
            }

            resolve(true)
          }, 500)
        })
      }

      // Show new content
      var launchStepSix = function () {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            jQuery('body').removeClass('launch-case-study')

            let urlSegments = url.split('/')
            let urlSegment = urlSegments[urlSegments.length - 2]  // trailing slash

            window.location.hash += urlSegment
            window.EventBus.$emit('caseStudyHasLoaded')
            resolve(true)
          }, 50)
        })
      }

      launchStepOne()
        .then(launchStepTwo)
        .then(launchStepThree)
        .then(launchStepFour)
        .then(launchStepFive)
        .then(launchStepSix)
    },
    launchMenu () {
      this.menuClasses.closed = false
      this.menuClasses.closing = false
      this.menuClasses.open = true
      this.menuClasses.opening = true

      jQuery('body').addClass('menu-open')
      jQuery('body').addClass('menu-opening')

      setTimeout(function () {
        this.menuClasses.opening = false
        jQuery('body').removeClass('menu-opening')
      }.bind(this), 700)
    },
    launchVideoFullscreen (url) {
      this.elements.videoFullscreen.visible = true
      this.elements.videoFullscreen.url = url
    },
    setupHeaderScrollInteraction () {
      this.scrollTop = jQuery(window).scrollTop()

      jQuery(window).scroll(function () {
        let currentScrollTop = jQuery(window).scrollTop()
        this.headerScrollToggle(currentScrollTop)
        this.headerOpacity(currentScrollTop)
        this.scrollTop = currentScrollTop
      }.bind(this))
    },
    headerOpacity(currentScrollTop) {
      if (jQuery('.page-header-large').length == 1) {
        let pageHeader = jQuery('.page-header-large')
        let endTransition = pageHeader.outerHeight()
        let startTransition = endTransition * 0.4

        // If the user's scroll position is past endTransition, it should be invisible
        if (currentScrollTop > endTransition) {
          pageHeader.css({
            opacity: 0
          })

        // If the user's scroll position is past startTransition, it should be visible
        } else if (currentScrollTop < startTransition) {
          pageHeader.css({
            opacity: 1
          })

        // Apply a custom opacity based on their location between startTransition and endTransition
        } else {
          let transitionDifference = Number(endTransition - startTransition)
          let currentDifference = Number(currentScrollTop - startTransition)
          let newOpacity = Number(1 - Number(currentDifference / transitionDifference))

          pageHeader.css({
            opacity: newOpacity
          })
        }
      }
    },
    headerScrollToggle (currentScrollTop) {
      if (currentScrollTop <= 2 || currentScrollTop >= this.scrollTop) {
        jQuery('body').removeClass('scroll-up')

        if (currentScrollTop > 2) {
          jQuery('body').addClass('scroll-down')

          if (this.scrollDir === 'up') {
            jQuery('#app > nav.navbar').attr('style', 'position: fixed;')
            jQuery('#app > nav.navbar').fadeOut(150)

            setTimeout(function () {
              jQuery('#app > nav.navbar').attr('style', '')
            }, 300)
          }
        }

        this.scrollDir = 'down'
      } else {
        this.scrollDir = 'up'
        jQuery('body').addClass('scroll-up')
        jQuery('body').removeClass('scroll-down')
      }
    },
    setupHeaderHeight () {
      let windowHeight = jQuery(window).height()
      let navHeight = jQuery('nav.navbar').outerHeight()

      jQuery('.page-header-large').css({
        height: windowHeight - navHeight + 'px'
      })
    },
    setupLeadershipTeamOverlay () {
      // Click to toggle
      jQuery('.showcase-leadershipteam .team-member').on('click', function () {
        if (jQuery(this).hasClass('active')) {
          jQuery('.showcase-leadershipteam .team-member.active').removeClass('active')
        } else {
          jQuery('.showcase-leadershipteam .team-member.active').removeClass('active')
          jQuery(this).addClass('active')
        }
      })

      // Mouse enter to show, with a slight delay for mobile mouseenter/click events
      jQuery('body:not(.iphone) .showcase-leadershipteam .team-member').on('mouseenter', function () {
        setTimeout(function () {
          if (!jQuery(this).hasClass('active')) {
            jQuery('.showcase-leadershipteam .team-member.active').removeClass('active')
            jQuery(this).addClass('active')
          }
        }.bind(this), 50)
      })

      // Mouse leave to hide
      jQuery('body:not(.iphone) .showcase-leadershipteam .team-member').on('mouseleave', function () {
        jQuery('.showcase-leadershipteam .team-member.active').removeClass('active')
      })
    },
    setupMasonryLayout () {
      let selector = '.masonry-grid'

      if (jQuery(selector).length > 0) {
        ImagesLoaded(selector, function () {
          let grid = document.querySelector(selector)
          let masonaryGrid = new Masonry(grid, {
            itemSelector: 'article',
            horizontalOrder: true,
            gutter: 30
          })

          masonaryGrid.layout()
        })
      }
    },
    setupWorkFilters () {
      var animation = { duration: 300 }

      // https://github.com/patrickkunka/mixitup/issues/321
      if (jQuery('body').hasClass('safari')) {
        animation = { enable: false }
      }

      if (jQuery('.page-template-template-work').length > 0) {
        mixitup('.page-template-template-work #content', {
          animation: animation,
          load: {
            filter: jQuery('.page-template-template-work .work-filter .filters').attr('data-first')
          }
        })
      }
    },
    storeUrlInLocalStorage () {
      localStorage.setItem('hw.currentUrl', window.location.href)
    },
    toggleClass (className, event) {
      console.log('toggleClass')
      jQuery(event.currentTarget).toggleClass(className)
    },
    toggleClassWithDelay (className, event) {
      console.log('toggleClassWithDelay')
      setTimeout(function () {
        this.toggleClass(className, event)
      }.bind(this), 1000)
    },
    toggleMenu () {
      if (this.menuClasses.closed) {
        this.launchMenu()
      } else if (this.menuClasses.open) {
        this.closeMenu()
      }
    }
  }
})

/**
 * Because we're not using a single-page app and this transition exists only for
 * the case studies, we need to add some custom functionality to detect whether
 * the user is going back or forward based on the hash
 *
 * https://gist.github.com/sstephenson/739659
 *
 * @param function onBack
 * @param function onForward
 * @return void
 */
var detectBackOrForward = function(onBack, onForward) {
  var hashHistory = [window.location.hash]
  var historyLength = window.history.length

  return function () {
    var hash = window.location.hash, length = window.history.length
    if (hashHistory.length && historyLength == length) {
      if (hashHistory[hashHistory.length - 2] == hash) {
        hashHistory = hashHistory.slice(0, -1)
        onBack()
      } else {
        hashHistory.push(hash)
        onForward()
      }
    } else {
      hashHistory.push(hash)
      historyLength = length
    }
  }
}

window.addEventListener('hashchange', detectBackOrForward(
  function() {
    window.location = document.location
  },
  function() {
    if (window.location.href.includes('#') === true) {
      window.location = window.location.href.replace('#', '')
    }
  }
))
