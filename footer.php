<?php
if (!defined('ABSPATH')) exit;

/**
 * The footer for Hotwire
 *
 * @package Hotwire
 */

$hasFooterLeft1 = is_active_sidebar('footer-left-1');
$hasFooterLeft2 = is_active_sidebar('footer-left-2');
$hasFooterBottom = is_active_sidebar('footer-bottom');
$customFooterBottom = get_field('footer_body');
$hasCustomFooterBottom = !empty($customFooterBottom);
$countryList = [];
$sessionCountryList = $_SESSION['hw_country_list'];

if (is_array($sessionCountryList) && !empty($sessionCountryList)) {
	$countryList = $sessionCountryList;
} else {
	$countryList = Hotwire_Country::get([
		'lang' => 'en',
	]);
}

$socialAccounts = Hotwire_Config::getSocialAccounts();
$currentTemplateFilename = get_page_template_slug(get_the_ID());
$hideFooterCountriesOnTemplate = ['template-contact.php'];

?>
		</main>
		<?php if (!in_array($currentTemplateFilename, $hideFooterCountriesOnTemplate)) { ?>
	    <section class="country-locations">
				<div class="container">
		      <h3>Global reach. <em>Local flavour.</em></h3>
					<div class="countries">
						<div class="inner">
							<?php if (is_array($countryList) && !empty($countryList)) { ?>
								<?php foreach ($countryList as $country) { ?>
									<div class="country">
									  <a href="<?php echo get_the_permalink($country->ID); ?>">
									    <?php echo $country->post_title; ?>
									  </a>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</div>
	    </section>
		<?php } ?>
		<footer id="footer">
			<section class="main <?php echo $hasFooterBottom || $hasCustomFooterBottom ? 'has-base' : null; ?>">
				<div class="container">
					<div class="row">
		        <div class="col-sm-6">
		          <h3>Get in touch.</h3>
		          <div class="row">
								<?php if ($hasFooterLeft1) { ?>
		            	<div class="col-md-6">
										<?php dynamic_sidebar('footer-left-1'); ?>
			            </div>
								<?php } ?>
								<?php if ($hasFooterLeft2) { ?>
			            <div class="col-md-6">
										<?php dynamic_sidebar('footer-left-2'); ?>
			            </div>
								<?php } ?>
		          </div>
		        </div>
		        <div class="col-sm-6">
							<?php if (is_array($socialAccounts) && !empty($socialAccounts)) { ?>
			          <div class="social">
									<?php foreach ($socialAccounts as $socialAccount) { ?>
										<a href="<?php echo $socialAccount['url']; ?>" target="_blank"
											title="Hotwire on <?php echo $socialAccount['type']; ?>">
											<span class="fa <?php echo $socialAccount['class']; ?>"></span>
										</a>
									<?php } ?>
								</div>
							<?php } ?>
							<div class="logo">
								<a href="<?php echo get_site_url(null, ''); ?>">
									<img src="<?php echo get_template_directory_uri() . '/assets/img/hotwire-logo-white.svg'; ?>" alt="Hotwire">
								</a>
							</div>
		        </div>
					</div>
				</div>
			</section>
			<?php if ($hasFooterBottom || $hasCustomFooterBottom) { ?>
				<section class="base">
					<div class="container">
						<?php if ($hasFooterBottom && !$customFooterBottom) { ?>
							<?php dynamic_sidebar('footer-bottom'); ?>
						<?php } ?>
						<?php if ($hasCustomFooterBottom) { ?>
							<?php echo $customFooterBottom; ?>
						<?php } ?>
					</div>
				</section>
			<?php } ?>
		</footer>
		<video-fullscreen
			v-if="elements.videoFullscreen.visible"
			:url="elements.videoFullscreen.url">
		</video-fullscreen>
	</div>
<?php wp_footer(); ?>
</body>
</html>
