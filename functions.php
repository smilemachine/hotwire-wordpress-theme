<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/inc/hotwire.php';
require_once dirname(__FILE__) . '/inc/hotwire/config.php';
require_once dirname(__FILE__) . '/inc/hotwire/helper.php';
require_once dirname(__FILE__) . '/inc/hotwire/autoloader.php';

if (defined('HW_IMPORT_CONTENT') && HW_IMPORT_CONTENT === true) {
  require_once dirname(__FILE__) . '/inc/hotwire/importer/class-hotwire-importer.php';
  require_once dirname(__FILE__) . '/inc/hotwire/importer/class-hotwire-importer-post.php';
  require_once dirname(__FILE__) . '/inc/hotwire/importer/class-hotwire-importer-user.php';
}

$autoloader = new Hotwire_Autoloader();
$autoloader->register();

$app = new Hotwire();
$app->setup();
