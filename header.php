<?php
if (!defined('ABSPATH')) exit;

/**
 * The header for Hotwire
 *
 * @package Hotwire
 */

the_post();

if (!session_id()) {
  session_start();
}

global $post;

// Get all the languages available for the current post
$languages = [];
$currentLanguage = null;
$currentLanguageCode = null;
$languageOptions = [];
$countryList = [];
$sessionCountryList = [];

if (isset($_SESSION['hw_country_list'])) {
  $sessionCountryList = $_SESSION['hw_country_list'];
}

if (function_exists('pll_the_languages')) {
	$languageOptions = pll_the_languages([
		'post_id' => get_the_ID(),
		'raw' => true,
	]);

	if (is_array($languageOptions) && !empty($languageOptions)) {
		foreach ($languageOptions as $key => $languageOption) {
			if (!$languageOption['no_translation']) {
				$languages[] = [
					'url' => $languageOption['url'],
					'name' => $languageOption['name'],
					'active' => $languageOption['current_lang'],
				];

				if ($languageOption['current_lang']) {
					$currentLanguage = $languageOption['name'];
					$currentLanguageCode = $languageOption['slug'];
				}
			}
		}
	}
}

if (is_array($sessionCountryList) && !empty($sessionCountryList)) {
	$countryList = $sessionCountryList;
} else {
	if (!empty($languageOptions) && function_exists('pll_get_post')) {
		$countries = Hotwire_Country::get([
			'lang' => 'en',
		]);

		if (is_array($countries) && !empty($countries)) {
			foreach ($countries as $country) {
				$countryToUse = $country;

				foreach ($languageOptions as $languageOption) {
					$translationID = pll_get_post($country->ID, $languageOption['slug']);

					if ($translationID !== false) {
						$countryToUse = get_post(intval($translationID));
					}
				}

				$countryList[$country->post_name] = $countryToUse;
			}
		}
	}

  ksort($countryList);

	$_SESSION['hw_country_list'] = $countryList;
}

$applyContentWidth = strtolower(get_field('content_width'));

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <!-- Lead forensics -->
  <script type="text/javascript" src="https://secure.leadforensics.com/js/107025.js"></script>
  <noscript><img src="https://secure.leadforensics.com/107025.png" style="display:none;" /></noscript>
  <!-- Lead forensics -->
	<div id="app" class="content-width-<?php echo $applyContentWidth; ?>">
		<nav class="navbar" :class="menuClasses">
			<div class="navbar-container navbar-default">
		    <div class="navbar-header">
					<button type="button" class="navbar-toggle" @click="toggleMenu">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
						<span class="sr-only">Menu</span>
		      </button>
		      <a class="navbar-brand" href="<?php echo get_site_url(null, ''); ?>"></a>
					<?php if (!is_front_page() && is_array($languages) && count($languages) > 1) { ?>
						<div class="navbar-language-switch btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="hidden-xs">
									<?php echo $currentLanguage; ?>
								</span>
								<span class="visible-xs">
									<?php echo $currentLanguageCode; ?>
								</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<?php foreach ($languages as $language) { ?>
							    <li>
										<a href="<?php echo $language['url']; ?>"
											class="<?php echo $language['active'] ? 'active' : null; ?>">
											<?php echo $language['name']; ?>
										</a>
									</li>
								<?php } ?>
							</ul>
						</div>
					<?php } ?>
				</div>
		  </div>
		</nav>
    <nav id="menu" class="menu" :class="menuClasses">
			<div class="menu-background-accent"></div>
			<div class="menu-background"></div>
			<div class="menu-background-base"></div>
			<div class="menu-content">
	      <?php
	        wp_nav_menu([
	          'menu' => 'menu-header',
	          'theme_location' => 'menu-header',
	          'depth' => 2,
	          'container' => 'div',
	          'container_class' => '',
	          'container_id' => '',
	          'menu_class' => 'nav navbar-nav',
						'lang' => 'en'
	        ]);
	      ?>
				<div class="menu-countries">
					<div class="inner">
						<?php if (is_array($countryList) && !empty($countryList)) { ?>
							<?php foreach ($countryList as $country) { ?>
								<?php $countryIsActive = strpos($country->post_name, $post->post_name) !== false; ?>
								<div class="country <?php echo $countryIsActive ? 'active' : null; ?>">
									<a href="<?php echo get_the_permalink($country->ID); ?>">
										<?php echo $country->post_title; ?>
									</a>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
    </nav>
		<main id="content">
