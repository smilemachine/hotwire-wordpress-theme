<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Hotwire
 *
 * @package Hotwire
 */
class Hotwire {

  const API_VERSION = 'v1';
  const API_NAMESPACE = 'hotwire/' . self::API_VERSION;
  const THEME = 'hotwire-wordpress-theme';

  protected $version;

  /**
   * Sets up the Hotwire theme
   */
  public function setup() {
    add_action('after_setup_theme', [$this, 'afterSetupTheme']);
    add_action('wp_enqueue_scripts', [$this, 'setupPublicStyles']);
    add_action('wp_enqueue_scripts', [$this, 'setupPublicScripts']);
    add_action('admin_enqueue_scripts', [$this, 'setupAdminStyles']);
    add_action('admin_menu', [$this, 'removeMenuItems']);
    add_action('admin_init' ,[$this, 'adminInit']);
    add_filter('oembed_dataparse', [$this, 'setupResponsiveVideos'], 10, 2);
    add_filter('body_class', [$this, 'addBodyClasses']);

    $this->setVersion();
    $this->setupCustomImageSizes();
    $this->setupCustomPostTypes();
    $this->setupCustomShortcodes();
    $this->setupCustomTaxonomies();
    $this->setupFooterWidgets();
    $this->setupThemeOptions();
    $this->setupTranslations();
    $this->importExistingContent();
  }

  /**
   * Retrieves the version of the current theme
   *
   * @return void
   */
  private function setVersion() {
    if ($theme = wp_get_theme()) {
      $this->version = $theme->get('Version');
    } else {
      $this->version = '0.0.1';
    }
  }

  /**
   * Returns the current version
   *
   * @return string
   */
  public function getVersion() {
    return $this->version;
  }

  /**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
   * @return void
   */
  public function afterSetupTheme() {

		/*
		 * Make theme available for translation which can be filed in /languages
		 */
		load_theme_textdomain(self::THEME, get_template_directory() . '/languages');

		/**
     * Add default posts and comments RSS feed links to head.
     */
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support('post-thumbnails');

		/**
     * This theme uses wp_nav_menu() in the following locations
     */
		register_nav_menus([
			'menu-header' => 'Header',
		]);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		]);
  }

  /**
   * Ads classes to the body
   * http://www.wpbeginner.com/wp-themes/how-to-add-user-browser-and-os-classes-in-wordpress-body-class/
   *
   * @param array $classes
   * @return void
   */
  public function addBodyClasses($classes = []) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone, $is_edge;

    if ($is_lynx) $classes[] = 'lynx';
    elseif ($is_gecko) $classes[] = 'gecko';
    elseif ($is_opera) $classes[] = 'opera';
    elseif ($is_NS4) $classes[] = 'ns4';
    elseif ($is_safari) $classes[] = 'safari';
    elseif ($is_chrome) $classes[] = 'chrome';
    elseif ($is_edge) $classes[] = 'edge';
    elseif ($is_IE) {
      $classes[] = 'ie';
      if (preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
      $classes[] = 'ie'.$browser_version[1];
    } else $classes[] = 'unknown';

    if ($is_iphone) $classes[] = 'iphone';

    if (stristr($_SERVER['HTTP_USER_AGENT'],'mac')) {
      $classes[] = 'osx';
    } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'linux')) {
      $classes[] = 'linux';
    } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'windows')) {
      $classes[] = 'windows';
    }

    // Edge and Firefox don't get detected properly sometimes
    // http://php.net/manual/en/function.get-browser.php#119332
    if (isset($_SERVER['HTTP_USER_AGENT'])) {
      $userAgent = $_SERVER['HTTP_USER_AGENT'];

      if (strpos($userAgent, 'Edge')) {
        $classes[] = 'edge';
      } else if (strpos($userAgent, 'Firefox')) {
        $classes[] = 'gecko';
      }
    }

    return $classes;
  }

  /**
   * Sets up the scripts for the public website
   *
   * @return void
   */
  public function setupPublicScripts() {
    // App
    wp_enqueue_script('hotwire-vendor-scripts', get_template_directory_uri() . '/assets/js/vendor.js', ['jquery', 'jquery-ui-core'], null, true);
    wp_enqueue_script('hotwire-app-scripts', get_template_directory_uri() . '/assets/js/main.js?' . $this->version, ['hotwire-vendor-scripts'], null, true);

    // Passle
    wp_enqueue_script('hotwire-passle-scripts', '//sdk.passle.net/jquery.passle.js', ['jquery'], null, true);
    wp_enqueue_script('hotwire-app-passle-scripts', get_template_directory_uri() . '/assets/js/passle.js', [], null, true);

    // Font Awesome
    wp_enqueue_script('hotwire-font-awesome', '//use.fontawesome.com/99423cfb8d.js', [], null, true);

    // URLs
    wp_localize_script('hotwire-app-scripts', 'HotwireUrls', self::getPublicUrls());

    // Typekit
    wp_enqueue_script('hotwire-typekit', '//use.typekit.net/cng3ese.js', [], null, false);

    // External
    wp_enqueue_script('hotwire-external', get_template_directory_uri() . '/assets/js/external.js', ['hotwire-typekit'], null, false);
  }

  /**
   * Sets up the styles for the admin area
   *
   * @return void
   */
  public function setupAdminStyles() {
    wp_enqueue_style('hotwire-admin-styles', get_template_directory_uri() . '/assets/css/admin.css', [], null, false);
  }

  /**
   * Returns an array of urls used for JS
   *
   * @return array
   */
  public function getPublicUrls() {
    return [
      'base' => get_site_url(null, '/'),
      'api' => get_site_url(null, '/wp-json/hotwire/v1/'),
    ];
  }

  /**
   * Sets up the styles for the public website
   *
   * @return void
   */
  public function setupPublicStyles() {
    wp_enqueue_style('hotwire-public-style', get_stylesheet_uri());
  	wp_enqueue_style('hotwire-public-styles', get_template_directory_uri() . '/assets/css/main.css?' . $this->version);
  }

  /**
   * Registers all the custom post types
   *
   * @return void
   */
  private function setupCustomPostTypes() {
    $country = new Hotwire_CTP_Country();
    $country->setup();

    $sector = new Hotwire_CTP_Sector();
    $sector->setup();

    $caseStudy = new Hotwire_CTP_CaseStudy();
    $caseStudy->setup();

    $award = new Hotwire_CTP_Award();
    $award->setup();

    $whitepaper = new Hotwire_CTP_Whitepaper();
    $whitepaper->setup();
  }

  /**
   * Registers all the custom shortcodes
   *
   * @return void
   */
  private function setupCustomShortcodes() {
    $caseStudies = new Hotwire_Shortcode_CaseStudies();
    $caseStudies->setup();
  }

  /**
   * Registers all the custom taxonomies
   *
   * @return void
   */
  private function setupCustomTaxonomies() {
    $service = new Hotwire_Taxonomy_Service();
    $service->setup();
  }

  /**
   * Registers the widgets for the footer
   *
   * @return void
   */
  private function setupFooterWidgets() {
    register_sidebar([
      'name' => 'Footer: Left 1',
      'id' => 'footer-left-1',
      'description' => 'Appears in the footer area',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
    ]);

    register_sidebar([
      'name' => 'Footer: Left 2',
      'id' => 'footer-left-2',
      'description' => 'Appears in the footer area',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
    ]);

    register_sidebar([
      'name' => 'Footer Bottom',
      'id' => 'footer-bottom',
      'description' => 'Bottom most footer. Typically contains copyright / disclaimer links.',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
    ]);
  }

  /**
   * Sets up ACF theme options
   *
   * @return void
   */
  public function setupThemeOptions() {
    if (function_exists('acf_add_options_page')) {
      acf_add_options_page([
        'page_title' => 'Hotwire Config',
        'menu_title' => 'Hotwire Config',
        'menu_slug' => 'hotwire-config',
        'capability' => 'edit_posts',
        'redirect' => false
      ]);
    }
  }

  /**
   * Registers translatable strings
   *
   * @return void
   */
  public function setupTranslations() {
    if (function_exists('pll_register_string') && is_admin()) {
      pll_register_string('case-study-related-title', 'Want more?', self::THEME);
      pll_register_string('case-study-back', 'Back', self::THEME);
      pll_register_string('case-study-global', 'Global', self::THEME);
      pll_register_string('case-study-campaign-overview', 'Campaign overview', self::THEME);
      pll_register_string('case-study-impact', 'Impact', self::THEME);
      pll_register_string('case-study-what-we-did', 'What we did', self::THEME);
      pll_register_string('case-study-results', 'Results', self::THEME);
      pll_register_string('thought-leadership-title', 'Thought Leadership.', self::THEME);
      pll_register_string('read-more', 'Read More', self::THEME);
    }
  }

  /**
   * Add custom image sizes, and update the actual wordpress ones
   *
   * @return void
   */
  private static function setupCustomImageSizes() {

    // Thumbnail for grid: 500x500
    update_option('thumbnail_size_w', 200);
    update_option('thumbnail_size_h', 200);
    update_option('thumbnail_crop', 0);

    // Medium for gallery: 1000x1000
    update_option('medium_size_w', 800);
    update_option('medium_size_h', 800);
    update_option('medium_crop', 0);

    // Large for full page: 1440x1440
    update_option('large_size_w', 1500);
    update_option('large_size_h', 850);
    update_option('large_crop', 1);

    // Initial image, for quick page load
    add_image_size('initial', 20, 20, true);
  }

  /**
   * Returns whether or not Hotwire is in dev mode
   *
   * @return bool
   */
  public static function inDevMode() {
    return true;
  }

  /**
   * Removes menu items
   *
   * @return void
   */
  public function removeMenuItems() {
    remove_menu_page('edit-comments.php');
  }

  /**
   * Add a wrapper around youtube embedded videos
   * https://lorut.no/responsive-vimeo-youtube-embed-wordpress/
   *
   * @param string $return
   * @param object $data
   * @param string $url
   * @return string
   */
  public function setupResponsiveVideos($html, $data) {
    if (!is_object($data) || empty($data->type)) {
      return $html;
    }

    // Verify that it is a video
    if (!($data->type == 'video')) {
      return $html;
    }

    $aspectRatio = $data->width / $data->height;
    $ratio4x3 = abs($aspectRatio - (4/3));
    $ratio16x9 = abs($aspectRatio - (16/9));
    $aspectRatioClass = ($ratio4x3 < $ratio16x9 ? 'embed-responsive-4by3' : 'embed-responsive-16by9');
    $html = preg_replace('/(width|height)="\d*"\s/', "", $html);
    $html = str_replace('https://', 'http://', $html);

    return '<div class="embed-responsive ' . $aspectRatioClass . '">' . $html . '</div>';
  }

  /**
   * Existing content import functionality to be used in development only
   */
  public function importExistingContent() {
    if (!defined('HW_IMPORT_CONTENT') || HW_IMPORT_CONTENT !== true) {
      return;
    }

    $importer = new Hotwire_Importer();
    $importer->setup();
  }

  /**
   * Admin init
   *
   * @return void
   */
  public function adminInit() {
    add_filter('manage_posts_columns', [$this, 'managePostsColumns']);
  }

  /**
   * Amend the posts columns
   *
   * @array $columns
   * @return void
   */
  public function managePostsColumns($columns) {
    unset($columns['tags']);
    return $columns;
  }
}
