<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Hotwire FlexibleContent: Showcase.Brands
 *
 * @package Hotwire
 */
class Hotwire_ACF_Page_Header {

  const TYPE_LARGE = 'Large';
  const TYPE_MEDIUM_ACCENT = 'Medium Accent';
  const TYPE_MEDIUM_STANDARD = 'Medium Standard';
  const TYPE_NO_IMAGE = 'No Image';

  /**
   * New class
   *
   * @return void
   */
  public function __construct() {
    //
  }

}
