<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Hotwire FlexibleContent: Showcase.Brands
 *
 * @package Hotwire
 */
class Hotwire_ACF_Showcase_Brands {

  protected $imageUrl;
  protected $items;
  protected $limit;

  /**
   * New class
   *
   * @return void
   */
  public function __construct() {
    $defaultLimit = 5;

    $this->limit = Hotwire_Helper::getSubField('brands_limit', $defaultLimit);
    $this->imageUrl = null;
    $this->items = null;

    if (!$this->limit || $this->limit == 0) {
      $this->limit = $defaultLimit;
    }
  }

  /**
   * Sets the items
   *
   * @param array $itemIDs
   * @return void
   */
  public function setItems($itemIDs = []) {
    $this->items = [];

    // We may be given a set of IDs to use
    if (is_array($itemIDs) && !empty($itemIDs)) {
      $items = Hotwire_CaseStudy::getWhereIn($itemIDs, [
        'orderby' => 'rand',
        'posts_per_page' => $this->limit
      ]);
    } else {
      // Use the user selected items if provided
      $items = get_sub_field('brands_items');

      // If they don't have any items set, get random ones
      if (!$items) {
        $items = Hotwire_CaseStudy::get([
          'orderby' => 'rand',
          'posts_per_page' => $this->limit
        ]);
      }
    }

    // Sets the items if they're valid
    if (is_array($items) && !empty($items)) {
      $this->items = $items;
    }
  }

  /**
   * Sets the image url
   *
   * @param string $imageUrl
   * @return void
   */
  public function setImageUrl($imageUrl = '') {
    if (!empty($imageUrl) && is_string($imageUrl)) {
      $this->imageUrl = $imageUrl;
    } else {
      $image = get_field('brands_image', 'option');

      if (!$image || !is_array($image) || empty($image)) {
        return get_template_directory_uri() . '/assets/img/showcase-brands-background.jpg';
      }

      $this->imageUrl = $image['url'];
    }
  }

  /**
   * Return the image url
   *
   * @return string
   */
  public function getImageUrl() {
    if (is_null($this->imageUrl)) {
      $this->setImageUrl();
    }

    if (!empty($this->imageUrl) && is_string($this->imageUrl)) {
      return $this->imageUrl;
    }

    return '';
  }

  /**
   * Get the items
   *
   * @return array
   */
  public function getItems() {
    // Set the items if they're not yet set
    if (is_null($this->items)) {
      $this->setItems();
    }

    // Return the items if they exist
    if (is_array($this->items) && !empty($this->items)) {
      $items = [];

      foreach ($this->items as $item) {
        $company = get_field('company', $item->ID);
        $quote = get_field('quote', $item->ID);

        if ($company && $quote) {
          $items[] = [
            'id' => $item->ID,
            'title' => $company,
            'body' => $quote,
            'url' => get_the_permalink($item->ID),
          ];
        }
      }

      return array_slice($items, 0, $this->limit);
    }

    return [];
  }

}
