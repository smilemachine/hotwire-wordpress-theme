<?php
if (!defined('ABSPATH')) exit;

/**
 * Setup for Hotwire FlexibleContent: Showcase.CaseStudy
 *
 * @package Hotwire
 */
class Hotwire_ACF_Showcase_CaseStudy {

  protected $title;
  protected $items;
  protected $limit;

  /**
   * New class
   *
   * @return void
   */
  public function __construct() {
    $defaultLimit = 3;
    $defaultTitle = 'Great ideas<br/>are our thing.';

    $this->title = Hotwire_Helper::getSubField('casestudy_title', $defaultTitle);
    $this->limit = Hotwire_Helper::getSubField('casestudy_limit', $defaultLimit);
    $this->items = null;

    if (!$this->limit || $this->limit == 0) {
      $this->limit = $defaultLimit;
    }
  }

  /**
   * Sets the items
   *
   * @param array $itemIDs
   * @return void
   */
  public function setItems($itemIDs = []) {
    $this->items = [];

    // We may be given a set of IDs to use
    if (is_array($itemIDs) && !empty($itemIDs)) {
      $items = Hotwire_CaseStudy::getWhereIn($itemIDs, [
        'orderby' => 'rand',
        'posts_per_page' => $this->limit
      ]);
    } else {
      // Use the user selected items if provided
      $items = get_sub_field('casestudy_items');

      // If they don't have any items set, get random ones
      if (!$items) {
        $items = Hotwire_CaseStudy::get([
          'orderby' => 'rand',
          'posts_per_page' => $this->limit
        ]);
      }
    }

    // Sets the items if they're valid
    if (is_array($items) && !empty($items)) {
      $this->items = $items;
    }
  }

  /**
   * Sets the title
   *
   * @param string $title
   * @return void
   */
  public function setTitle($title) {
    if (!empty($title) && is_string($title)) {
      $this->title = $title;
    }
  }

  /**
   * Return the title
   *
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Get the item IDs
   *
   * @return array
   */
  public function getItemIDs() {
    $itemIDs = [];

    // Set the items if they're not yet set
    if (is_null($this->items)) {
      $this->setItems();
    }

    // Return an array of items if they're valid
    if (is_array($this->items) && !empty($this->items)) {
      foreach ($this->items as $item) {
        if (is_int($item)) {
          $itemIDs[] = $item;
        } else {
          $itemIDs[] = $item->ID;
        }
      }
    }

    return $itemIDs;
  }

}
