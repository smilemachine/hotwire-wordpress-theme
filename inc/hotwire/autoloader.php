<?php
if (!defined('ABSPATH')) exit;

/**
 * Autoloads Hotwire classes using WordPress convention.
 *
 * @package Hotwire
 * @author https://carlalexander.ca/organizing-files-object-oriented-wordpress-plugin
 */
class Hotwire_Autoloader {

  /**
   * Registers Autoloader as an SPL autoloader
   *
   * @param boolean $prepend
   */
  public function register($prepend = false) {
    if (version_compare(phpversion(), '5.3.0', '>=')) {
      spl_autoload_register(array(new self, 'autoloadCustomPostTypes'), true, $prepend);
      spl_autoload_register(array(new self, 'autoloadCustomTaxonomies'), true, $prepend);
      spl_autoload_register(array(new self, 'autoloadModels'), true, $prepend);
      spl_autoload_register(array(new self, 'autoloadCustomShortcodes'), true, $prepend);
      spl_autoload_register(array(new self, 'autoloadACFContent'), true, $prepend);
      spl_autoload_register(array(new self, 'autoloadServices'), true, $prepend);
    } else {
      spl_autoload_register(array(new self, 'autoloadCustomPostTypes'));
      spl_autoload_register(array(new self, 'autoloadCustomTaxonomies'));
      spl_autoload_register(array(new self, 'autoloadModels'));
      spl_autoload_register(array(new self, 'autoloadCustomShortcodes'));
      spl_autoload_register(array(new self, 'autoloadACFContent'));
      spl_autoload_register(array(new self, 'autoloadServices'));
    }
  }

  /**
   * Handles autoloading of Hotwire files
   *
   * @param string $folder
   * @param string $class
   * @return void
   */
  private function autoloadByFolder($folder, $class) {
    if (0 !== strpos($class, 'Hotwire')) {
      return;
    }

    $path = dirname(__FILE__) . '/' . $folder . '/';
    $filename = 'class-' . strtolower(str_replace(array('_', "\0"), array('-', ''), $class)) . '.php';

    if (is_file($file = $path . $filename)) {
      require_once $file;
    }
  }

  /**
   * Handles autoloading of custom post types
   *
   * @param string $class
   * @return void
   */
  public function autoloadCustomPostTypes($class) {
    self::autoloadByFolder('custom-post-types', $class);
  }

  /**
   * Handles autoloading of shortcodes
   *
   * @param string $class
   * @return void
   */
  public function autoloadCustomShortcodes($class) {
    self::autoloadByFolder('custom-shortcodes', $class);
  }

  /**
   * Handles autoloading of taxonomies
   *
   * @param string $class
   * @return void
   */
  public function autoloadCustomTaxonomies($class) {
    self::autoloadByFolder('custom-taxonomies', $class);
  }

  /**
   * Handles autoloading of ACF content
   *
   * @param string $class
   * @return void
   */
  public function autoloadACFContent($class) {
    self::autoloadByFolder('acf', $class);
  }

  /**
   * Handles autoloading of models
   *
   * @param string $class
   * @return void
   */
  public function autoloadModels($class) {
    self::autoloadByFolder('models', $class);
  }

  /**
   * Handles autoloading of services
   *
   * @param string $class
   * @return void
   */
  public function autoloadServices($class) {
    self::autoloadByFolder('services', $class);
  }

}
