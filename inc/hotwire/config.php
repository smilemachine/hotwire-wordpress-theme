<?php
if (!defined('ABSPATH')) exit;

/**
 * Helper functions for Hotwire
 *
 * @package Hotwire
 */
class Hotwire_Config {

  /**
   * Returns a list of expertises available for the front-end
   *
   * @return array
   */
  public static function getExpertiseSectors() {
    $items = get_field('expertise_sectors', 'option');
    $sectors = [];

    if (!is_array($items) || empty($items)) {
      return [];
    }

    foreach ($items as $item) {
      $sector = $item['sector'];
      $sectors[] = [
        'title' => $item['title'],
        'sector' => $sector ? $sector->ID : null
      ];
    }

    return $sectors;
  }

  /**
   * Returns the social accounts available for the front-end
   *
   * @return array
   */
  public static function getSocialAccounts() {
    $accounts = [];
    $items = get_field('social_accounts', 'option');

    if (!is_array($items) || empty($items)) {
      return [];
    }

    foreach ($items as $item) {
      switch ($item['type']) {
        case 'Linked In':
          $accounts[] = [
            'url' => $item['url'],
            'type' => $item['type'],
            'class' => 'fa-linkedin',
          ];
          break;
        case 'Twitter':
          $accounts[] = [
            'url' => $item['url'],
            'type' => $item['type'],
            'class' => 'fa-twitter',
          ];
          break;
        case 'Facebook':
          $accounts[] = [
            'url' => $item['url'],
            'type' => $item['type'],
            'class' => 'fa-facebook',
          ];
          break;
        case 'Google Plus':
          $accounts[] = [
            'url' => $item['url'],
            'type' => $item['type'],
            'class' => 'fa-google-plus-official',
          ];
          break;
        case 'YouTube':
          $accounts[] = [
            'url' => $item['url'],
            'type' => $item['type'],
            'class' => 'fa-youtube-play',
          ];
          break;
        case 'Instagram':
          $accounts[] = [
            'url' => $item['url'],
            'type' => $item['type'],
            'class' => 'fa-instagram',
          ];
          break;
      }
    }

    return $accounts;
  }

}
