<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-hotwire-ctp.php';

/**
 * Awards CTP
 *
 * @package Hotwire
 */
class Hotwire_CTP_Award implements Hotwire_CTP {

  const POST_TYPE = 'hw_award';

  /**
   * Setup the Award CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [$this, 'register']);
  }

  /**
   * Registers Award CTP
   *
   * @return void
   */
  public function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Awards',
        'singular_name' => 'Award',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Award',
        'edit_item' => 'Edit Award',
        'new_item' => 'New Award',
        'view_item' => 'View Award',
        'view_items' => 'View Awards',
        'search_items' => 'Search Awards',
        'not_found' => 'No awards found',
        'not_found_in_trash' => 'No awards found in trash',
        'parent_item_colon' => 'Parent Award',
        'all_items' => 'All Awards',
        'archives' => 'Award Archives',
        'attributes' => 'Award Attributes',
        'insert_into_item' => 'Insert into award',
        'upload_to_this_item' => 'Insert into award',
      ],
      'public' => false,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true,
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-awards',
      'hierarchical' => false,
      'supports' => ['title', 'thumbnail'],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'award',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

}
