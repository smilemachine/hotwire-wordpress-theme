<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-hotwire-ctp.php';

/**
 * CaseStudy CTP
 *
 * @package Hotwire
 */
class Hotwire_CTP_CaseStudy implements Hotwire_CTP {

  const POST_TYPE = 'hw_casestudy';

  /**
   * Setup the CaseStudy CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [$this, 'register']);
  }

  /**
   * Registers CaseStudy CTP
   *
   * @return void
   */
  public function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Case Studies',
        'singular_name' => 'Case Study',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Case Study',
        'edit_item' => 'Edit Case Study',
        'new_item' => 'New Case Study',
        'view_item' => 'View Case Study',
        'view_items' => 'View Case Studies',
        'search_items' => 'Search Case Studies',
        'not_found' => 'No case studies found',
        'not_found_in_trash' => 'No case studies found in trash',
        'parent_item_colon' => 'Parent Case Study',
        'all_items' => 'All Case Studies',
        'archives' => 'Case Study Archives',
        'attributes' => 'Case Study Attributes',
        'insert_into_item' => 'Insert into case study',
        'upload_to_this_item' => 'Insert into case study',
      ],
      'public' => true,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true,
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-book-alt',
      'hierarchical' => false,
      'supports' => ['title', 'thumbnail', 'revisions'],
      'taxonomies' => [Hotwire_Taxonomy_Service::TAXONOMY],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'work',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

}
