<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-hotwire-ctp.php';

/**
 * Country CTP
 *
 * @package Hotwire
 */
class Hotwire_CTP_Country implements Hotwire_CTP {

  const POST_TYPE = 'hw_country';

  /**
   * Setup the Country CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [$this, 'register']);
  }

  /**
   * Registers Country CTP
   *
   * @return void
   */
  public function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Countries',
        'singular_name' => 'Country',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Country',
        'edit_item' => 'Edit Country',
        'new_item' => 'New Country',
        'view_item' => 'View Country',
        'view_items' => 'View Countries',
        'search_items' => 'Search Countries',
        'not_found' => 'No countries found',
        'not_found_in_trash' => 'No countries found in trash',
        'parent_item_colon' => 'Parent Country',
        'all_items' => 'All Countries',
        'archives' => 'Country Archives',
        'attributes' => 'Country Attributes',
        'insert_into_item' => 'Insert into country',
        'upload_to_this_item' => 'Insert into country',
      ],
      'public' => true,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true,
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-location',
      'hierarchical' => false,
      'supports' => ['title', 'editor', 'thumbnail', 'revisions'],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'country',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

}
