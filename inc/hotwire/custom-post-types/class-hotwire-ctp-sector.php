<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-hotwire-ctp.php';

/**
 * Sectors CTP
 *
 * @package Hotwire
 */
class Hotwire_CTP_Sector implements Hotwire_CTP {

  const POST_TYPE = 'hw_sector';

  /**
   * Setup the Sector CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [$this, 'register']);
  }

  /**
   * Registers Sector CTP
   *
   * @return void
   */
  public function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Sectors',
        'singular_name' => 'Sector',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Sector',
        'edit_item' => 'Edit Sector',
        'new_item' => 'New Sector',
        'view_item' => 'View Sector',
        'view_items' => 'View Sectors',
        'search_items' => 'Search Sectors',
        'not_found' => 'No sectors found',
        'not_found_in_trash' => 'No sectors found in trash',
        'parent_item_colon' => 'Parent Sector',
        'all_items' => 'All Sectors',
        'archives' => 'Sector Archives',
        'attributes' => 'Sector Attributes',
        'insert_into_item' => 'Insert into sector',
        'upload_to_this_item' => 'Insert into sector',
      ],
      'public' => TRUE,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true,
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-chart-bar',
      'hierarchical' => false,
      'supports' => ['title', 'editor', 'thumbnail'],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'sector',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

}
