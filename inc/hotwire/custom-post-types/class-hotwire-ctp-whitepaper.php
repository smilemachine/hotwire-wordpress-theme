<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-hotwire-ctp.php';

/**
 * Whitepapers CTP
 *
 * @package Hotwire
 */
class Hotwire_CTP_Whitepaper implements Hotwire_CTP {

  const POST_TYPE = 'hw_whitepaper';

  /**
   * Setup the Whitepaper CTP
   *
   * @return void
   */
  public function setup() {
    add_action('init', [$this, 'register']);
  }

  /**
   * Registers Whitepaper CTP
   *
   * @return void
   */
  public function register() {
    register_post_type(self::POST_TYPE, [
      'labels' => [
        'name' => 'Whitepapers',
        'singular_name' => 'Whitepaper',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Whitepaper',
        'edit_item' => 'Edit Whitepaper',
        'new_item' => 'New Whitepaper',
        'view_item' => 'View Whitepaper',
        'view_items' => 'View Whitepapers',
        'search_items' => 'Search Whitepapers',
        'not_found' => 'No whitepapers found',
        'not_found_in_trash' => 'No whitepapers found in trash',
        'parent_item_colon' => 'Parent Whitepaper',
        'all_items' => 'All Whitepapers',
        'archives' => 'Whitepaper Archives',
        'attributes' => 'Whitepaper Attributes',
        'insert_into_item' => 'Insert into whitepaper',
        'upload_to_this_item' => 'Insert into whitepaper',
      ],
      'public' => true,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true,
      'show_in_admin_bar' => false,
      'menu_position' => 20,
      'menu_icon' => 'dashicons-format-aside',
      'hierarchical' => false,
      'supports' => ['title', 'editor', 'thumbnail'],
      'has_archive' => false,
      'rewrite' => [
        'slug' => 'whitepaper',
        'with_front' => false,
        'feeds' => false,
        'pages' => false,
      ]
    ]);
  }

}
