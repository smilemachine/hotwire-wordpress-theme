<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom post type interface
 *
 * @package Hotwire
 */
interface Hotwire_CTP {
  public function setup();
  function register();
}
