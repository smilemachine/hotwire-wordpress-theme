<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom shortcodes
 *
 * @package CaseStudy
 */
class Hotwire_Shortcode_CaseStudies {

  public function setup() {
    add_shortcode('hw_casestudies', [self::class, 'render']);
  }

  /**
   * Renders given case-study cards
   *
   * @param array $params
   * @return string
   */
  public static function render($params) {
    $items = self::getItemsFromParams($params);

    // Ensure the file exists
    $shortcodePath = dirname(__FILE__) . '/views/case-studies.php';

    if (!file_exists($shortcodePath)) {
      return;
    }

    ob_start();
    include $shortcodePath;
    return ob_get_clean();
  }

  /**
   * Retrieves the items from the params
   *
   * @param array $params
   * @return string
   */
  private static function getItemsFromParams($params = []) {
    $items = [];

    if (isset($params['ids'])) {
      $array = array_map('intval', array_map('trim', explode(',', $params['ids'])));
      $items = Hotwire_CaseStudy::getWhereIn($array);
    }

    return $items;
  }

}
