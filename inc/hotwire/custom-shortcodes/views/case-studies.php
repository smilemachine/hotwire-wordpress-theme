<?php
if (!defined('ABSPATH')) exit;

/**
 * Case studies shortcode
 *
 * @package Hotwire
 */

$i = 1;
$perRow = 3;
$totalItems = count($items);

/**
 * Determines how many items will be rendered for this row
 *
 * @param integer $i
 * @param integer $totalItems
 * @param integer $perRow
 * @return integer
 */
function hotwireCaseStudiesItemsInRow($i, $totalItems, $perRow) {
  $items = $totalItems + 1 - $i;

  if ($items > $perRow) {
    return $perRow;
  }

  return $items;
}

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <div class="casestudies">
    <div class="row x<?php echo hotwireCaseStudiesItemsInRow($i, $totalItems, $perRow); ?>">
      <?php foreach ($items as $item) { ?>
        <?php
          $company = get_field('company', $item->ID);
          $campaign = get_field('campaign_title', $item->ID);
          $countryAbbr = Hotwire_Helper::getTranslation('Global');

          $imageInitialUrl = Hotwire_Helper::getPostThumbnailUrl($item->ID, 'initial');
          $imageFullUrl = Hotwire_Helper::getPostThumbnailUrl($item->ID, 'medium');
          $countries = get_field('country', $item->ID);

          if (is_array($countries) && !empty($countries)) {
            // If we're on an country page, show the country abbreviation
            if (get_post_type() == Hotwire_CTP_Country::POST_TYPE) {
              foreach ($countries as $country) {
                if ($country->ID == get_the_ID()) {
                  $countryAbbr = get_field('abbreviation', $country->ID);
                  break;
                }
              }

            // If there's only one country, show that abbreviation
            } else if (count($countries) <= 2) {
              $countryAbbrs = [];

              foreach ($countries as $country) {
                $countryAbbrs[] = get_field('abbreviation', $country->ID);
              }

              $countryAbbr = implode('/', $countryAbbrs);
            }
          }
        ?>
        <article class="case-study">
          <a href="<?php echo get_permalink($item->ID); ?>">
            <slick-image
              :initial="'<?php echo $imageInitialUrl; ?>'"
              :full="'<?php echo $imageFullUrl; ?>'">
            </slick-image>
            <section class="information">
              <span class="country"><?php echo $countryAbbr; ?></span>
              <span class="company"><?php echo $company; ?></span>
              <span class="campaign"><?php echo $campaign; ?></span>
            </section>
            <section class="content">
              <span class="title"><?php echo $item->post_title; ?></span>
            </section>
          </a>
        </article>
        <?php if ($i % $perRow == 0) { ?>
          <?php $itemsInRow = hotwireCaseStudiesItemsInRow($i + 1, $totalItems, $perRow); ?>
          <?php if ($itemsInRow > 0) { ?>
            </div><div class="row x<?php echo $itemsInRow; ?>">
          <?php } ?>
        <?php } ?>
        <?php $i++; ?>
      <?php } ?>
    </div>
  </div>
<?php } ?>
