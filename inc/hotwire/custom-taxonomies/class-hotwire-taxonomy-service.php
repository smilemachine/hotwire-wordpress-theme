<?php
if (!defined('ABSPATH')) exit;

require_once dirname(__FILE__) . '/interface-hotwire-taxonomy.php';

/**
 * Service custom taxonomy
 *
 * @package Hotwire
 */
class Hotwire_Taxonomy_Service implements Hotwire_Taxonomy {

  const TAXONOMY = 'hw_taxonomy_service';

  /**
   * Registers Service as a custom taxonomy
   *
   * @return void
   */
  public function setup() {
    $params = [
      'labels' => [
        'name' => 'Services',
        'singular_name' => 'Service',
        'search_items' => 'Search Services',
    		'popular_items' => 'Popular Services',
    		'all_items' => 'All Services',
    		'parent_item' => null,
    		'parent_item_colon' => null,
    		'edit_item' => 'Edit Service',
    		'update_item' => 'Update Service',
    		'add_new_item' => 'Add New Service',
    		'new_item_name' => 'New Service Name',
    		'separate_items_with_commas' => 'Separate services with commas',
    		'add_or_remove_items' => 'Add or remove services',
    		'choose_from_most_used' => 'Choose from the most used services',
    		'not_found' => 'No services found.',
    		'menu_name' => 'Services',
      ],
      'hierarchical' => false,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => [
        'slug' => 'service',
        'with_front' => false,
        'hierarchical' => false,
      ],
    ];

    $postTypes = [
      Hotwire_CTP_CaseStudy::POST_TYPE,
    ];

    register_taxonomy(self::TAXONOMY, $postTypes, $params);
  }

}
