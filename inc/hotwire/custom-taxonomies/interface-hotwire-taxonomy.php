<?php
if (!defined('ABSPATH')) exit;

/**
 * Custom post type interface
 *
 * @package Hotwire
 */
interface Hotwire_Taxonomy {
  public function setup();
}
