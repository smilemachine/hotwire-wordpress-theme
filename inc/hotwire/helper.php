<?php
if (!defined('ABSPATH')) exit;

/**
 * Helper functions for Hotwire
 *
 * @package Hotwire
 */
class Hotwire_Helper {

  /**
   * Converts an array of posts to an array of ID => post_title
   *
   * @param array $posts
   * @return array
   */
  public static function postsToList($posts) {
    $list = [];

    if (is_array($posts) && !empty($posts)) {
      foreach ($posts as $item) {
        if (isset($item->ID) && isset($item->post_title)) {
          $list[$item->ID] = $item->post_title;
        }
      }
    }

    return $list;
  }

  /**
   * Converts an array of posts to an array of ID => post_title
   *
   * @param array $items
   * @param string $valueKey
   * @param string $keyKey
   * @return array
   */
  public static function arrayToList($items, $valueKey, $keyKey = null) {
    $list = [];

    if (is_array($items) && !empty($items)) {
      foreach ($items as $item) {
        if (!isset($item[$valueKey])) {
          continue;
        }

        if (!is_null($keyKey) && isset($item[$keyKey])) {
          $list[$item[$keyKey]] = $item[$valueKey];
        } else {
          $list[] = $item[$valueKey];
        }
      }
    }

    return $list;
  }

  /**
   * Returns an array of valid IDs, aka an integer > 0
   *
   * @param array $array
   * @return array
   */
  public static function validIDsFromArray($array) {
    $postIDs = [];

    if (!is_array($array) || empty($array)) {
      return $postIDs;
    }

    foreach ($array as $item) {
      $id = intval($item);

      if ($id > 0) {
        $postIDs[] = $id;
      }
    }

    return $postIDs;
  }

  /**
   * Returns a merged array of parameters for a query
   *
   * @param array $defaultParams
   * @param array $customParams
   * @return array
   */
  public static function paramsForQuery($defaultParams = [], $customParams = []) {
    if (!is_array($defaultParams) || !is_array($customParams)) {
      return [];
    }

    return array_merge($defaultParams, $customParams);
  }

  /**
   * Transforms an array into an object ready for echoing to JS
   *
   * @param array $array
   * @return string
   */
  public static function arrayToObjectString($array) {
    if (!is_array($array)) {
      return '';
    }

    return str_replace('"', "'", str_replace("'", "\'", json_encode($array)));
  }

  /**
   * Returns a rendered template part with local variables
   *
   * @param string $template
   * @param array $data
   * @return void;
   */
  public static function getTemplatePart($template, $data = []) {
    $filename = $template . '.php';

    if (empty(locate_template($filename)) || !is_array($data)) {
      return '';
    }

    extract($data);

    ob_start();
    include(locate_template($filename));
    return ob_get_clean();
  }

  /**
   * Highlights the last symbol in the given string
   *
   * @param string $str
   * @return string
   */
  public static function highlightSymbols($str) {
    if (!is_string($str)) {
      return '';
    }

    $symbols = ['.', '?', '!'];
    $lastChar = substr($str, -1);

    if (!in_array($lastChar, $symbols)) {
      return $str;
    }

    $highlight = '<span class="highlight">' . $lastChar . '</span>';
    $normal = substr($str, 0, -1);

    return $normal . $highlight;
  }

  /**
   * Returns the ACF field, or a default value if empty
   *
   * @param string $field
   * @param mixed $default
   * @return mixed
   */
  public static function getField($field, $default = '') {
    if (is_array($default)) {
      $acfField = get_field($field);
      return is_array($acfField) ? $acfField : $default;
    } else if (is_int($default)) {
      $acfField = intval(get_field($field));
      return $acfField > 0 ? $acfField : $default;
    } else {
      $acfField = get_field($field);
      return !empty($acfField) ? $acfField : $default;
    }
  }

  /**
   * Returns the ACF sub field, or a default value if empty
   *
   * @param string $field
   * @param mixed $default
   * @return mixed
   */
  public static function getSubField($field, $default = '') {
    if (is_array($default)) {
      $acfField = get_sub_field($field);
      return is_array($acfField) ? $acfField : $default;
    } else if (is_int($default)) {
      return intval(get_sub_field($field));
    } else {
      $acfField = get_sub_field($field);
      return !empty($acfField) ? $acfField : $default;
    }
  }

  /**
   * Returns the url for a posts image at a given size
   *
   * @param integer $postID
   * @param string $size
   * @return string
   */
  public static function getPostThumbnailUrl($postID, $size) {
    $thumbnailID = get_post_thumbnail_id(intval($postID));
    $imageArray = wp_get_attachment_image_src($thumbnailID, $size);

    if (!is_array($imageArray) || !isset($imageArray[0])) {
      return '';
    }

    return $imageArray[0];
  }

  /**
   * Returns the slug for the given string
   * https://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
   *
   * @param type $string
   * @return void
   */
  public static function getSlug($type) {
    $text = preg_replace('~[^\pL\d]+~u', '-', $type); // replace non letter or digits by -
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text); // transliterate
    $text = preg_replace('~[^-\w]+~', '', $text); // remove unwanted characters
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text); // remove duplicate -
    $text = strtolower($text); // lowercase

    if (empty($text)) {
      return '';
    }

    return $text;
  }

  /**
   * Determines if we are wanting to load a page via ajax
   *
   * @return bool
   */
  public static function isAjaxRequest() {
    return is_array($_GET) && !empty($_GET) && isset($_GET['ajax']) && $_GET['ajax'] === 'true';

    return false;
  }

  /**
   * Write the given string to the given file path
   *
   * @param string $string
   * @param string $filename
   * @return void
   */
  public static function writeStringToUploads($string, $filename) {
    $wpUploadDir = wp_upload_dir();

    if (isset($wpUploadDir['basedir'])) {
      $file = fopen($wpUploadDir['basedir'] . '/' . $filename, 'w');

      fwrite($file, $string);
      fclose($file);
    }
  }

  /**
   * ToDo: Store Instagram, Twitter and Greenhouse feeds locally
   * Determines if a local feed has expired
   *
   * @param string $filename
   * @param integer $hours
   * @return mixed
   */
  public static function fetchUnexpiredLocalFeed($filename, $hours = 2) {
    $wpUploadDir = wp_upload_dir();
    $expiryTimestamp = strtotime('NOW - ' . intval($hours) . ' hours');

    if (isset($wpUploadDir['basedir'])) {
      $feed = @file_get_contents($wpUploadDir['basedir'] . '/' . $filename);
      $jsonFeed = json_decode($feed);

      if ($jsonFeed && isset($jsonFeed->timestamp) && isset($jsonFeed->feed)
          && $jsonFeed->timestamp > $expiryTimestamp) {
          return $jsonFeed->feed;
      }
    }

    return null;
  }

  /**
   * Gets the translated version of the string
   *
   * @param string $string
   * @return string
   */
  public static function getTranslation($string) {
    if (function_exists('pll__')) {
      return pll__($string);
    }

    return $string;
  }

}
