<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Hotwire: Post
 *
 * @package Hotwire
 */
class Hotwire_Importer_Post {

  public function __construct() {
    //
  }

  /**
   * Import all posts
   *
   * @param array $ids
   * @return void
   */
  public function importExistingPosts($ids = []) {
    $existing = $this->getExistingPosts($ids);

    if (is_array($existing) && !empty($existing)) {
      foreach ($existing as $post) {
        $this->createPostFromExistingPost($post);
      }
    }
  }

  /**
   * Create a new post from an existing source
   *
   * @param stdClass $existingPost
   * @return boolean
   */
  private function createPostFromExistingPost($existingPost) {
    // User
    $user = get_user_by('email', strtolower($existingPost->user_email));

    if (!$user) {
      $iUser = new Hotwire_Importer_User();
      $userID = $iUser->createUserFromExistingEmail($existingPost->user_email);

      if (!is_int($userID) || $userID == 0) {
        return;
      }
    }

    // Content
    $content = $existingPost->content;

    if (!empty($existingPost->meta_body_0)) {
      $content = $existingPost->meta_body_0;
    }

    if (!empty($existingPost->meta_body_1)) {
      $content = $existingPost->meta_body_1;
    }

    if (!empty($existingPost->meta_body_2)) {
      $content = $existingPost->meta_body_2;
    }

    if (!empty($existingPost->meta_body_3)) {
      $content = $existingPost->meta_body_3;
    }

    $content = str_replace('hotwirepr.com', 'hotwireglobal.com', $content);

    // Title
    $title = $existingPost->title;

    if (!empty($existingPost->meta_title_0)) {
      $title = $existingPost->meta_title_0;
    }

    if (!empty($existingPost->meta_title_1)) {
      $title = $existingPost->meta_title_1;
    }

    if (!empty($existingPost->meta_title_2)) {
      $title = $existingPost->meta_title_2;
    }

    // Set the data
    $data = [
      'post_author' => $user->ID,
      'post_date' => $existingPost->published,
      'post_content' => $content,
      'post_title' => $title,
      'post_excerpt' => '',
      'post_status' => 'publish',
      'post_type' => 'post',
      'comment_status' => 'closed',
      'ping_status' => 'closed',
    ];

    // Check if this post already exists
    $posts = get_posts([
      'posts_per_page' => 1,
      'post_type' => 'post',
      'meta_key' => Hotwire_Importer::KEY_IMPORT_ID,
      'meta_value' => $existingPost->id,
    ]);

    // If it doesn't exist, create it and store the original ID
    if (!empty($posts)) {
      $data['ID'] = $posts[0]->ID;
    }

    $postID = wp_insert_post($data);

    update_post_meta($postID, Hotwire_Importer::KEY_IMPORT_ID, $existingPost->id);

    $post = get_post($postID);

    // Save the meta data
    $this->savePostContent($post, $existingPost);
  }

  /**
   * Save the content tabs for the given post
   *
   * @param WP_Post $post
   * @param stdClass $existingPost
   * @return void
   */
  private function savePostContent($post, $existingPost) {
    $title = $post->post_title;
    $subtitle = '';

    if (!empty($existingPost->meta_subtitle_0)) {
      $subtitle = $existingPost->meta_subtitle_0;
    }

    if (!empty($existingPost->meta_subtitle_1)) {
      $subtitle = $existingPost->meta_subtitle_1;
    }

    if (!empty($existingPost->meta_subtitle_2)) {
      $subtitle = $existingPost->meta_subtitle_2;
    }

    update_field('field_599561ad2b8fc', 'No Image', $post->ID);
    update_field('field_5995640a2b8fd', $title, $post->ID);
    update_field('field_5995641f2b8fe', $subtitle, $post->ID);
  }

  /**
   * Get existing IDs
   *
   * @param array $ids
   * @return array
   */
  public function getExistingPostIDs($after = 0) {
    $db = Hotwire_Importer::originalDatabaseConnection();

    return $db->get_results('
      SELECT
        `p`.`ID` AS `id`

      FROM `htw_posts` `p`

      WHERE `p`.`ID` > ' . intval($after) . '

      ORDER BY `p`.`ID` ASC

      LIMIT 10
    ');
  }

  /**
   * Get existing posts
   *
   * @param array $ids
   * @return array
   */
  public function getExistingPosts($ids = []) {
    $db = Hotwire_Importer::originalDatabaseConnection();
    $idsSql = '';

    if (is_array($ids) && !empty($ids)) {
      $ids = array_map('intval', $ids);
      $idsSql = 'AND `p`.`ID` IN (' . implode(',', $ids) . ')';
    }

    return $db->get_results('
      SELECT
        `p`.`ID` AS `id`,
        `p`.`post_content` AS `content`,
        `p`.`post_title` AS `title`,
        `p`.`post_name` AS `slug`,
        `p`.`post_date` AS `published`,
        `m_title_0`.`meta_value` AS `meta_title_0`,
        `m_subtitle_0`.`meta_value` AS `meta_subtitle_0`,
        `m_body_0`.`meta_value` AS `meta_body_0`,
        `m_title_1`.`meta_value` AS `meta_title_1`,
        `m_subtitle_1`.`meta_value` AS `meta_subtitle_1`,
        `m_body_1`.`meta_value` AS `meta_body_1`,
        `m_title_2`.`meta_value` AS `meta_title_2`,
        `m_subtitle_2`.`meta_value` AS `meta_subtitle_2`,
        `m_body_2`.`meta_value` AS `meta_body_2`,
        `m_body_3`.`meta_value` AS `meta_body_3`,
        `seo_title`.`meta_value` AS `seo_title`,
        `seo_body`.`meta_value` AS `seo_body`,
        `u`.`user_email` AS `user_email`

      FROM `htw_posts` `p`

      LEFT JOIN `htw_postmeta` `m_title_0`
        ON `m_title_0`.`post_id` = `p`.`ID`
        AND `m_title_0`.`meta_key` = "content_0_title"

      LEFT JOIN `htw_postmeta` `m_subtitle_0`
        ON `m_subtitle_0`.`post_id` = `p`.`ID`
        AND `m_subtitle_0`.`meta_key` = "content_0_teaser_text"

      LEFT JOIN `htw_postmeta` `m_body_0`
        ON `m_body_0`.`post_id` = `p`.`ID`
        AND `m_body_0`.`meta_key` = "content_0_body"

      LEFT JOIN `htw_postmeta` `m_title_1`
        ON `m_title_1`.`post_id` = `p`.`ID`
        AND `m_title_1`.`meta_key` = "content_1_title"

      LEFT JOIN `htw_postmeta` `m_subtitle_1`
        ON `m_subtitle_1`.`post_id` = `p`.`ID`
        AND `m_subtitle_1`.`meta_key` = "content_1_teaser_text"

      LEFT JOIN `htw_postmeta` `m_body_1`
        ON `m_body_1`.`post_id` = `p`.`ID`
        AND `m_body_1`.`meta_key` = "content_1_body"

      LEFT JOIN `htw_postmeta` `m_title_2`
        ON `m_title_2`.`post_id` = `p`.`ID`
        AND `m_title_2`.`meta_key` = "content_2_title"

      LEFT JOIN `htw_postmeta` `m_subtitle_2`
        ON `m_subtitle_2`.`post_id` = `p`.`ID`
        AND `m_subtitle_2`.`meta_key` = "content_2_teaser_text"

      LEFT JOIN `htw_postmeta` `m_body_2`
        ON `m_body_2`.`post_id` = `p`.`ID`
        AND `m_body_2`.`meta_key` = "content_2_body"

      LEFT JOIN `htw_postmeta` `m_body_3`
        ON `m_body_3`.`post_id` = `p`.`ID`
        AND `m_body_3`.`meta_key` = "content_3_paragraph"

      LEFT JOIN `htw_postmeta` `seo_title`
        ON `seo_title`.`post_id` = `p`.`ID`
        AND `seo_title`.`meta_key` = "_yoast_wpseo_title"

      LEFT JOIN `htw_postmeta` `seo_body`
        ON `seo_body`.`post_id` = `p`.`ID`
        AND `seo_body`.`meta_key` = "_yoast_wpseo_opengraph-description"

      LEFT JOIN `htw_users` `u`
        ON `u`.`ID` = `p`.`post_author`

      WHERE `p`.`post_type` = "post"
        AND `p`.`post_status` = "publish"
        AND `p`.`post_parent` = 0
        ' . $idsSql . '

      LIMIT 1
    ');
  }

}
