<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Hotwire: Post
 *
 * @package Hotwire
 */
class Hotwire_Importer_User {

  public function __construct() {
    //
  }

  /**
   * Create a new user from an existing email address
   *
   * @param string $existingEmail
   * @return integer
   */
  public function createUserFromExistingEmail($existingEmail) {
    $existingUser = $this->getExistingUserFromEmail($existingEmail);

    if (!$existingUser) {
      return 0;
    }

    $userRole = null;
    $userRoles = unserialize($existingUser->role);

    if (is_array($userRoles) && !empty($userRoles)) {
      foreach ($userRoles as $key => $value) {
        if ($value === true) {
          $userRole = $key;
          break;
        }
      }
    }

    if (!$userRole) {
      return 0;
    }

    return wp_insert_user([
      'user_pass' => uniqid(),
      'user_login' => $existingUser->user_login,
      'user_nicename' => $existingUser->user_nicename,
      'user_email' => strtolower($existingUser->user_email),
      'display_name' => $existingUser->display_name,
      'nickname' => $existingUser->nickname,
      'first_name' => $existingUser->first_name,
      'last_name' => $existingUser->last_name,
      'role' => $userRole,
      'show_admin_bar_front' => false,
    ]);
  }

  /**
   * Get existing posts
   *
   * @param string $existingEmail
   * @return array
   */
  public function getExistingUserFromEmail($existingEmail) {
    $db = Hotwire_Importer::originalDatabaseConnection();

    return $db->get_row('
      SELECT
        `u`.`*`,
        `m_role`.`meta_value` AS `role`

      FROM `htw_users` `u`

      LEFT JOIN `htw_usermeta` `m_role`
        ON `m_role`.`user_id` = `u`.`ID`
        AND `m_role`.`meta_key` = "htw_capabilities"

      WHERE `u`.`user_email` = "' . $existingEmail . '"
    ');
  }

}
