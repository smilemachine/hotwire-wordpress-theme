<?php
if (!defined('ABSPATH')) exit;

/**
 * Content import for Hotwire
 *
 * @package Hotwire
 */
class Hotwire_Importer {

  const KEY_IMPORT_ID = 'HW_IMPORT_ID';

  public function __construct() {
    //
  }

  public function setup() {
    if (isset($_GET['import'])) {
      $function = $_GET['importFunction'];
      $ids = [];

      if (isset($_GET['importIDs'])) {
        $ids = array_map('trim', explode(',', $_GET['importIDs']));
      }

      if (method_exists(self::class, $function)) {
        $this->{$function}($ids);
      }
    }

    return;
  }

  /**
   * Returns a wpdb connection for the original database
   *
   * @return mixed
   */
  public static function originalDatabaseConnection() {
    if (defined('HW_IMPORT_DB_NAME') && defined('HW_IMPORT_DB_USER') && defined('HW_IMPORT_DB_PASSWORD')) {
      $wpdb = new wpdb(HW_IMPORT_DB_USER, HW_IMPORT_DB_PASSWORD, HW_IMPORT_DB_NAME, 'localhost');

      if ($wpdb) {
        return $wpdb;
      }
    }

    return null;
  }

  /**
   * Import all posts
   *
   * @param string $language
   * @param array $ids
   * @return void
   */
  private function importPosts($ids = []) {
    $importer = new Hotwire_Importer_Post($language);
    $postIDs = [];

    if (!is_array($ids) || empty($ids)) {
      $posts = $importer->getExistingPostIDs();
    } else {
      $importer->importExistingPosts($ids);

      // Get the next IDs to import
      $after = intval(end($ids));
      $posts = $importer->getExistingPostIDs($after);
    }

    foreach ($posts as $post) {
      $postIDs[] = $post->id;
    }

    if (!empty($postIDs)) {
      $url = get_site_url(null) . '/?' . http_build_query([
        'import' => true,
        'importFunction' => 'importPosts',
        'importIDs' => implode(',',  $postIDs)
      ]);

      // Errors with too many redirects using header
      echo '<script>window.location = "' . $url . '";</script>'; exit;
    } else {
      die('All posts imported.');
    }
  }

}
