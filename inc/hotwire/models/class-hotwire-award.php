<?php
if (!defined('ABSPATH')) exit;

/**
 * Award model
 *
 * @package Hotwire
 */
class Hotwire_Award {

  /**
   * Returns an array of Awards
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_type' => Hotwire_CTP_Award::POST_TYPE,
      'posts_per_page' => 5,
      'orderby' => 'rand',
    ];

    // If limit is set to 0, transform it to infinite
    if (isset($params['posts_per_page']) && intval($params['posts_per_page']) <= 0) {
      $params['posts_per_page'] = -1;
    }

    $params = Hotwire_Helper::paramsForQuery($defaultParams, $customParams);
    $posts = get_posts($params);

    if ($returnAsList !== true) {
      return $posts;
    }

    return Hotwire_Helper::postsToList($posts);
  }

  /**
   * Returns an array of Awards in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = Hotwire_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = Hotwire_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Get an array of image urls
   *
   * @param array $items
   * @return array
   */
  public static function getImageURLs($items) {
    $urls = [];

    if (is_array($items) && !empty($items)) {
    	foreach ($items as $item) {
    		$urls[] = Hotwire_Helper::getPostThumbnailUrl($item->ID, 'thumbnail');
    	}
    }

    return $urls;
  }

}
