<?php
if (!defined('ABSPATH')) exit;

/**
 * CaseStudy model
 *
 * @package Hotwire
 */
class Hotwire_CaseStudy {

  /**
   * Returns an array of Case Studies
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_type' => Hotwire_CTP_CaseStudy::POST_TYPE,
      'posts_per_page' => -1,
    ];

    // If limit is set to 0, transform it to infinite
    if (isset($params['posts_per_page']) && intval($params['posts_per_page']) <= 0) {
      $params['posts_per_page'] = -1;
    }

    $params = Hotwire_Helper::paramsForQuery($defaultParams, $customParams);
    $posts = get_posts($params);

    if ($returnAsList !== true) {
      return $posts;
    }

    return Hotwire_Helper::postsToList($posts);
  }

  /**
   * Returns an array of Case Studies in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = Hotwire_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = Hotwire_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of Case Studies that are not in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereNotIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = Hotwire_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__not_in' => $postIDs,
    ];
    $params = Hotwire_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of related Case Studies for the given ID.
   * If that country has a translation, only return case-studies that
   * have that same translation.
   *
   * For example:
   *  - Viewing a case study in German, show German case studies
   *  - Viewing a case study in English that also has a German version, show German case studies
   *
   * @param integer $postID
   * @param boolean $returnAsList
   * @return array
   */
  public static function getRelated($postID, $returnAsList = false) {
    $countryIDs = [];
    $countries = get_field('country', $postID);

    // Get the translated versions of the countries
    if (function_exists('pll_the_languages')) {
      if (is_array($countries) && !empty($countries)) {
        foreach ($countries as $country) {
          $languageOptions = pll_the_languages([
            'post_id' => $country->ID,
            'raw' => true,
          ]);

          foreach ($languageOptions as $languageOption) {
            if ($languageOption['no_translation']) {
              continue;
            }

            $countryIDs[] = pll_get_post($country->ID, $languageOption['slug']);
          }
        }
      }
    }

    $metaQuery = ['relation' => 'OR'];

    foreach ($countryIDs as $countryID) {
      $metaQuery[] = [
        'key' => 'country',
        'value' => '"' . $countryID . '"',
        'compare' => 'LIKE',
      ];

      $metaQuery[] = [
        'key' => 'country',
        'value' => $countryID,
        'compare' => '=',
      ];
    }

    return self::getWhereNotIn([$postID], [
      'posts_per_page' => 3,
      'meta_query' => $metaQuery,
    ], $returnAsList);
  }

}
