<?php
if (!defined('ABSPATH')) exit;

/**
 * Sector model
 *
 * @package Hotwire
 */
class Hotwire_Sector {

  /**
   * Returns an array of Sectors
   *
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function get($customParams = [], $returnAsList = false) {
    $defaultParams = [
      'post_type' => Hotwire_CTP_Sector::POST_TYPE,
      'posts_per_page' => -1,
    ];

    // If limit is set to 0, transform it to infinite
    if (isset($params['posts_per_page']) && intval($params['posts_per_page']) <= 0) {
      $params['posts_per_page'] = -1;
    }

    $params = Hotwire_Helper::paramsForQuery($defaultParams, $customParams);
    $posts = get_posts($params);

    if ($returnAsList !== true) {
      return $posts;
    }

    return Hotwire_Helper::postsToList($posts);
  }

  /**
   * Returns an array of Sectors in an array of IDs
   *
   * @param array $rawIDs
   * @param array $customParams
   * @param boolean $returnAsList
   * @return array
   */
  public static function getWhereIn($rawIDs, $customParams = [], $returnAsList = false) {
    $postIDs = Hotwire_Helper::validIDsFromArray($rawIDs);

    if (empty($postIDs)) {
      return [];
    }

    $defaultParams = [
      'post__in' => $postIDs,
    ];
    $params = Hotwire_Helper::paramsForQuery($defaultParams, $customParams);

    return self::get($params, $returnAsList);
  }

  /**
   * Returns an array of related Case Studies for the given ID
   *
   * @param integer $postID
   * @param boolean $returnAsList
   * @return array
   */
  public static function getCaseStudies($postID, $returnAsList = false) {
    return Hotwire_CaseStudy::get([
      'orderby' => 'rand',
      'posts_per_page' => 3,
      'meta_key' => 'sector',
      'meta_value' => $postID,
    ], $returnAsList);
  }

}
