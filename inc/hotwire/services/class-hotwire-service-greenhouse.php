<?php
if (!defined('ABSPATH')) exit;

/**
 * Greenhouse service for Hotwire
 *
 * @package Hotwire
 */
class Hotwire_Service_Greenhouse {

  private $baseUrl;

  /**
   * Setup
   *
   * @return void
   */
  public function __construct() {
    $this->baseUrl = 'https://api.greenhouse.io/v1/boards';
  }

  /**
   * Gets the jobs feed
   *
   * @return array
   */
  public function getFeed() {
    $filename = 'hw-jobs.json';
    $unexpiredLocalFeed = Hotwire_Helper::fetchUnexpiredLocalFeed($filename, 2);

    if ($unexpiredLocalFeed) {
      return $unexpiredLocalFeed;
    }

    $items = [
      'america' => [
        'title' => 'North America',
        'usernames' => ['hotwireprus'],
        'jobs' => [],
      ],
      'europe' => [
        'title' => 'Europe',
        'usernames' => ['hwl', 'hwp', 'hotwirede', 'hotwirees', 'hotwireit'],
        'jobs' => [],
      ],
      'australia' => [
        'title' => 'Australia',
        'usernames' => ['hotwireau'],
        'jobs' => [],
      ],
    ];

    foreach ($items as $key => $region) {
      $jobs = $this->getFeedForUsernames($region['usernames']);
      $items[$key]['jobs'] = $jobs;
    }

    $feedString = json_encode([
      'timestamp' => strtotime('NOW'),
      'feed' => $items,
    ]);

    Hotwire_Helper::writeStringToUploads($feedString, $filename);

    return Hotwire_Helper::fetchUnexpiredLocalFeed($filename);
  }

  /**
   * Gets the feed for the given Greenhouse usernames
   *
   * @param array $usernames
   * @return array
   */
  public function getFeedForUsernames($usernames = []) {
    $jobs = [];

    if (!is_array($usernames) || empty($usernames)) {
      return $jobs;
    }

    foreach ($usernames as $username) {
      $usernameFeed = $this->getFeedForUsername($username);

      if (is_array($usernameFeed)) {
        $jobs = array_merge($jobs, $usernameFeed);
      }
    }

    return $jobs;
  }


  public function getFeedForUsername($username) {
    $filename = 'hw-jobs-' . $username . '.json';
    $unexpiredLocalFeed = Hotwire_Helper::fetchUnexpiredLocalFeed($filename, 2);

    if ($unexpiredLocalFeed) {
      return $unexpiredLocalFeed;
    }

    $items = [];
    $url = $this->baseUrl . '/' . $username . '/jobs/';
    $feed = @file_get_contents($url);

    if ($feed) {
      $decodedFeed = json_decode($feed);

      if (is_array($decodedFeed->jobs) && !empty($decodedFeed->jobs)) {
        foreach ($decodedFeed->jobs as $job) {
          $locations = array_map('trim', explode(',', $job->location->name));

          if (!empty($locations)) {
            foreach ($locations as $location) {
              $items[] = [
                'id' => $job->id,
                'url' => get_site_url(null, '/careers/job/' . $job->id),
                'username' => $username,
                'title' => $job->title,
              ];
            }
          }
        }
      }
    }

    $feedString = json_encode([
      'timestamp' => strtotime('NOW'),
      'feed' => $items,
    ]);

    Hotwire_Helper::writeStringToUploads($feedString, $filename);

    return Hotwire_Helper::fetchUnexpiredLocalFeed($filename);
  }

  /**
   * Gets a single job
   *
   * @param integer $id
   * @return array
   */
  public function getJob($id) {
    $jobFeed = $this->getFeed();

    if ($jobFeed) {
      foreach ($jobFeed as $region) {
        if (isset($region->jobs) && is_array($region->jobs) && !empty($region->jobs)) {
          foreach ($region->jobs as $job) {
            if (isset($job->id) && isset($job->username)) {
              if (intval($job->id) == intval($id)) {
                $url = $this->baseUrl . '/' . $job->username . '/jobs/' . $id;
                $feed = @file_get_contents($url);
                break;
              }
            }
          }
        }
      }
    }

    if ($feed) {
      return json_decode($feed);
    }

    return null;
  }

}
