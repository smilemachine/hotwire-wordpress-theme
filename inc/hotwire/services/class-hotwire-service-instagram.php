<?php
if (!defined('ABSPATH')) exit;

/**
 * Instagram service for Hotwire
 *
 * @package Hotwire
 */
class Hotwire_Service_Instagram {

  private $username;
  private $filename;

  public function __construct($username) {
    $this->username = $username;
    $this->filename = 'hw-instagram-' . Hotwire_Helper::getSlug($username) . '.json';
  }

  /**
   * Gets the instagram feed
   *
   * @param integer $limit
   * @return array
   */
  public function getFeed($limit = 12) {
    $unexpiredLocalFeed = Hotwire_Helper::fetchUnexpiredLocalFeed($this->filename, 1);

    if ($unexpiredLocalFeed && is_array($unexpiredLocalFeed) && !empty($unexpiredLocalFeed)) {
      return $unexpiredLocalFeed;
    }

    $items = [];
    $feed = @file_get_contents('https://www.instagram.com/' . $this->username . '/media');

    if ($feed) {
      $decodedFeed = json_decode($feed);

      if (isset($decodedFeed->items) && is_array($decodedFeed->items) && !empty($decodedFeed->items)) {
        foreach ($decodedFeed->items as $key => $value) {
          if ($key >= $limit) {
            break;
          }

          $items[] = [
            'url' => $value->link,
            'imageUrl' => $value->images->standard_resolution->url,
          ];
        }
      }
    }

    $feedString = json_encode([
      'timestamp' => strtotime('NOW'),
      'feed' => $items,
    ]);

    Hotwire_Helper::writeStringToUploads($feedString, $this->filename);

    return Hotwire_Helper::fetchUnexpiredLocalFeed($this->filename);
  }

}
