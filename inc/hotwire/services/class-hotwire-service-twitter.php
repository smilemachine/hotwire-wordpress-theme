<?php
if (!defined('ABSPATH')) exit;

/**
 * Twitter service for Hotwire
 *
 * @package Hotwire
 */
class Hotwire_Service_Twitter {

  private $username;
  private $filename;

  public function __construct($username) {
    $this->username = $username;
    $this->filename = 'hw-twitter-' . Hotwire_Helper::getSlug($username) . '.json';
  }

  /**
   * Gets the latest tweet ids for a username
   * https://gist.github.com/lastguest/b933e8164b7a71f6c488
   *
   * @param integer $limit
   * @return array
   */
  public function getTweetIDs($limit = 3) {
    $unexpiredLocalFeed = Hotwire_Helper::fetchUnexpiredLocalFeed($this->filename, 1);

    if ($unexpiredLocalFeed && is_array($unexpiredLocalFeed) && !empty($unexpiredLocalFeed)) {
      return $unexpiredLocalFeed;
    }

    $tweetIDs = [];
    $page = @file_get_contents('https://mobile.twitter.com/' . $this->username);
    preg_match_all(
    '{<div class="tweet-text" data-id="(\d+)">\s*<div class="dir-ltr" dir="ltr">\s*(.+?)\s*</div>\s*</div>}', $page, $matches);

    if (!isset($matches[1]) || empty($matches[1])) {
      return [];
    }

    if (count($matches[1]) < $limit) {
      $tweetIDs = array_values($matches[1]);
    } else {
      $tweetIDs = array_slice(array_values($matches[1]), 0, $limit);
    }

    $feedString = json_encode([
      'timestamp' => strtotime('NOW'),
      'feed' => $tweetIDs,
    ]);

    Hotwire_Helper::writeStringToUploads($feedString, $this->filename);

    return Hotwire_Helper::fetchUnexpiredLocalFeed($this->filename);
  }

}
