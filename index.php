<?php
if (!defined('ABSPATH')) exit;

/**
 * The main template file
 *
 * @package Hotwire
 */

?>
<?php get_header(); ?>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/page-header'); ?>
<div class="container">
	<?php
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				get_template_part('template-parts', get_post_format());
			}
		} else {
			get_template_part('template-parts/content', 'none');
		}
	?>
</div>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/index'); ?>
<?php get_footer(); ?>
