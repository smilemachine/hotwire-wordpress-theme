<?php
if (!defined('ABSPATH')) exit;

/**
 * The main template file for a Case Study
 *
 * @package Hotwire
 */

$headerTitle = get_the_title();
$headerSubtitle = get_field('company');
$headerType = Hotwire_ACF_Page_Header::TYPE_MEDIUM_STANDARD;

$campaignOverview = get_field('campaign_overview');
$campaignStatistics = get_field('campaign_statistics');
$campaignStatisticsHeading = get_field('campaign_statistics_heading');
$campaignHeading = get_field('campaign_heading');

if (!$campaignHeading || empty($campaignHeading)) {
  $campaignHeading = Hotwire_Helper::getTranslation('Campaign overview');
}

if (!$campaignStatisticsHeading || empty($campaignStatisticsHeading)) {
  $campaignStatisticsHeading = Hotwire_Helper::getTranslation('Impact');
}

$whatWeDid = get_field('what_we_did');
$whatWeDidTitle = get_field('what_we_did_title');

if (!$whatWeDidTitle || empty($whatWeDidTitle)) {
  $whatWeDidTitle = Hotwire_Helper::getTranslation('What we did');
}

$results = get_field('results');
$resultsTitle = get_field('results_title');

if (!$resultsTitle || empty($resultsTitle)) {
  $resultsTitle = Hotwire_Helper::getTranslation('Results');
}

$bodyImage = get_field('body_image');

$quote = get_field('quote');
$quotee = get_field('quotee');
$quoteeTitle = get_field('quotee_title');

$externalUrl = get_field('external_url');
$externalUrlTitle = get_field('external_url_title');

if (!$externalUrlTitle || empty($externalUrlTitle)) {
  $externalUrlTitle = Hotwire_Helper::getTranslation('Visit Site');
}

$awards = get_field('awards');

$related = Hotwire_CaseStudy::getRelated(get_the_ID(), true);
$relatedIDs = array_keys($related);

$ajax = Hotwire_Helper::isAjaxRequest();

?>
<?php
  if (!$ajax) {
    get_header();

    echo Hotwire_Helper::getTemplatePart('template-parts/page-header', [
      'type' => $headerType,
      'title' => $headerTitle,
      'subtitle' => $headerSubtitle,
      'showGradient' => true,
    ]);
  }
?>
<section class="campaign-overview">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 overview">
        <header>
          <h3>
            <?php echo $campaignHeading; ?>
          </h3>
        </header>
        <div class="content">
          <?php echo $campaignOverview; ?>
        </div>
      </div>
      <aside class="col-sm-4 col-md-3 col-md-offset-1 statistics">
        <header>
          <h3>
            <?php echo $campaignStatisticsHeading; ?>
          </h3>
        </header>
        <div class="content">
          <ul>
            <?php foreach ($campaignStatistics as $statistic) { ?>
              <li>
                <span class="value">
                  <?php echo $statistic['number']; ?>
                </span>
                <span class="title">
                  <?php echo $statistic['title']; ?>
                </span>
              </li>
            <?php } ?>
          </ul>
        </div>
      </aside>
    </div>
  </div>
</section>
<section class="campaign-content">
  <div class="container">
    <section class="what-we-did">
      <header>
        <h3>
          <?php echo $whatWeDidTitle; ?>
        </h3>
      </header>
      <div class="content">
        <?php echo apply_filters('the_content', $whatWeDid); ?>
      </div>
    </section>
    <?php if (is_array($bodyImage) && !empty($bodyImage)) { ?>
      <section class="image">
        <img src="<?php echo $bodyImage['sizes']['medium']; ?>">
      </section>
    <?php } ?>
    <section class="results">
      <header>
        <h3>
          <?php echo $resultsTitle; ?>
        </h3>
      </header>
      <div class="content">
        <?php echo apply_filters('the_content', $results); ?>
      </div>
    </section>
    <?php if (!empty($externalUrl)) { ?>
      <section class="external-link">
        <a href="<?php echo $externalUrl; ?>" target="_blank" class="btn btn-link">
          <?php echo $externalUrlTitle; ?>
        </a>
      </section>
    <?php } ?>
    <?php if (!empty($quote)) { ?>
      <section class="quote">
        <blockquote class="quote">
          "<?php echo $quote; ?>"
        </blockquote>
        <?php if (!empty($quotee)) { ?>
          <span class="quotee">
            <?php echo $quotee . (!empty($quoteeTitle) ? ',' : null); ?>
          </span>
        <?php } ?>
        <?php if (!empty($quoteeTitle)) { ?>
          <span class="quotee-title">
            <?php echo $quoteeTitle; ?>
          </span>
        <?php } ?>
      </section>
    <?php } ?>
    <?php if (is_array($awards) && !empty($awards)) { ?>
      <section class="awards">
        <?php foreach ($awards as $award) { ?>
          <div>
            <?php $awardImageUrl = Hotwire_Helper::getPostThumbnailUrl($award->ID, 'medium'); ?>
            <img src="<?php echo $awardImageUrl; ?>" alt="<?php echo $award->post_title; ?>">
          </div>
        <?php } ?>
      </section>
    <?php } ?>
  </div>
</section>
<?php if (is_array($relatedIDs) && !empty($relatedIDs)) { ?>
  <section class="related">
    <?php
      echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/showcase-case-studies', [
        'title' => Hotwire_Helper::getTranslation('Want more?'),
        'itemIDs' => $relatedIDs
      ]);
    ?>
  </section>
<?php } ?>
<?php get_template_part('template-parts/acf-content/index'); ?>
<?php
  if (!$ajax) {
    get_footer();
  }
?>
