<?php
if (!defined('ABSPATH')) exit;

/**
 * The main template file for a Country
 *
 * @package Hotwire
 */

$introTitle = get_field('intro_title');

if (empty($introTitle)) {
  $introTitle = 'Welcome.';
}

$introBody = get_field('intro_body');
$introImage = get_field('intro_image');

// Get the translated country ids
$languageIDs = [];

if (function_exists('pll_the_languages')) {
  $languageOptions = pll_the_languages([
    'post_id' => get_the_ID(),
    'raw' => true,
  ]);

  foreach ($languageOptions as $languageOption) {
    if ($languageOption['no_translation']) {
      break;
    }

    $languageIDs[] = pll_get_post(get_the_ID(), $languageOption['slug']);
  }
}

/**
 * We want to show the translated versions of all of this countries posts
 * Some may be linked directly to the translation, whilst others may be linked
 * only to the english version
 */
$caseStudyMetaQuery = ['relation' => 'OR'];

foreach ($languageIDs as $languageID) {
  $caseStudyMetaQuery[] = [
    'key' => 'country',
    'value' => '"' . $languageID . '"',
    'compare' => 'LIKE',
  ];
}

$caseStudyTitle = get_field('casestudy_title');
$caseStudyItemIDs = [];
$caseStudies = Hotwire_CaseStudy::get([
  'posts_per_page' => 3,
  'meta_query' => $caseStudyMetaQuery,
]);

foreach ($caseStudies as $caseStudy) {
  $caseStudyItemIDs[] = $caseStudy->ID;
}

$brandsImageUrl = '';
$brandsImage = get_field('brands_showcase_image');
$brandsItems = get_field('brands_showcase_items');

if (is_array($brandsImage)) {
  $brandsImageUrl = $brandsImage['url'];
}

$instagramName = get_field('instagram_name');
$twitterName = get_field('twitter_name');

$thoughtLeadershipFilter = get_field('thought_leadership_filter');
$thoughtLeadershipTitle = get_field('thought_leadership_title');
$thoughtLeadershipReadMore = get_field('thought_leadership_read_more');

$contactTitle = get_field('contact_title');
$contactName = get_field('contact_subtitle');
$contactImage = get_field('contact_image');
$contactBody = get_field('contact_body');
$contactLocations = get_field('contact_locations');
$contactLocationCount = count($contactLocations);
$contactColumnClass = '';

if ($contactLocationCount == 1) {
  $contactColumnClass = 'col-sm-6';
} else if ($contactLocationCount == 2) {
  $contactColumnClass = 'col-sm-6 col-md-4';
} else if ($contactLocationCount == 3) {
  $contactColumnClass = 'col-sm-6 col-md-6 col-lg-3';
}

$contactFormPardotURL = get_Field('pardot_form_url');

$mapLat = (float) get_field('map_latitude');
$mapLng = (float) get_field('map_longitude');
$mapZoom = intval(get_field('map_zoom'));
$mapIcon = get_template_directory_uri() . '/assets/img/icon-map-marker.png';

$whitepaper = get_field('whitepaper');
$whitepaperButton = get_field('whitepaper_button');
$whitepaperButtonURL = get_field('whitepaper_button_url');

$jobsTitle = get_field('jobs_title');
$jobsNoPositions = get_field('jobs_no_positions');
$jobsUsernamesString = get_field('jobs_usernames');
$jobsUsernames = [];
$jobsReadMore = get_field('jobs_read_more');

if (empty($jobsTitle)) {
  $jobsTitle = 'Current openings.';
}

if (empty($jobsNoPositions)) {
  $jobsNoPositions = 'Nothing here? We’d still love to hear from you. Drop a note to us above and we’ll be in touch soon.';
}

if (!empty($jobsUsernamesString)) {
  $jobsUsernames = array_map('trim', explode(',', $jobsUsernamesString));
}

if (empty($jobsReadMore)) {
  $jobsReadMore = 'Find out more';
}

?>
<?php get_header(); ?>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/page-header'); ?>
<section class="intro style-light">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-lg-5">
        <header>
          <h2><?php echo $introTitle; ?></h2>
        </header>
        <p><?php echo $introBody; ?></p>
      </div>
      <div class="col-sm-6 col-lg-5 col-lg-offset-2">
        <img src="<?php echo $introImage['sizes']['medium']; ?>" title="<?php echo $introTitle; ?>">
      </div>
    </div>
  </div>
</section>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/showcase-case-studies', [
    'title' => $caseStudyTitle,
    'itemIDs' => $caseStudyItemIDs,
  ]);
?>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/showcase-brands', [
    'imageUrl' => $brandsImageUrl,
    'itemIDs' => $brandsItems,
  ]);
?>
<?php
  if ($whitepaper) {
    echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/call-to-action-whitepaper', [
      'button' => $whitepaperButton,
      'whitepaper' => $whitepaper,
      'buttonUrl' => $whitepaperButtonURL,
      'style' => 'gray'
    ]);
  }
?>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/feed-thought-leadership', [
    'title' => $thoughtLeadershipTitle,
    'filter' => $thoughtLeadershipFilter,
    'readMoreText' => $thoughtLeadershipReadMore,
    'limit' => 3
  ]);
?>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/feed-instagram', [
    'username' => $instagramName,
    'style' => 'light',
  ]);
?>
<?php
  if (is_array($jobsUsernames) && !empty($jobsUsernames)) {
    echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/feed-jobs', [
      'noPositionsGlobal' => $jobsNoPositions,
      'noPositionsRegion' => $jobsNoPositions,
      'readMore' => $jobsReadMore,
      'title' => $jobsTitle,
      'usernames' => $jobsUsernames,
      'style' => 'white'
    ]);
  }
?>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/feed-twitter', [
    'username' => $twitterName,
  ]);
?>
<section class="contact style-gray">
  <div class="container">
    <header>
      <h2><?php echo $contactTitle; ?></h2>
    </header>
    <div class="details locations-<?php echo $contactLocationCount; ?>">
      <div class="row">
        <div class="<?php echo $contactColumnClass; ?>">
          <div class="inner">
            <div class="details-image" style="background-image: url('<?php echo $contactImage['sizes']['medium']; ?>');">
            </div>
            <div class="details-info">
              <?php if (!empty($contactName)) { ?>
                <h4 class="subtitle">
                  <?php echo $contactName; ?>
                </h4>
              <?php } ?>
              <div class="content">
                <?php echo $contactBody; ?>
              </div>
            </div>
          </div>
        </div>
        <?php if (is_array($contactLocations) && !empty($contactLocations)) { ?>
          <?php foreach ($contactLocations as $contactLocation) { ?>
            <?php
              $mapLat = (float) $contactLocation['map_latitude'];
              $mapLng = (float) $contactLocation['map_longitude'];
              $mapLocation = '{ lat: ' . $mapLat . ', lng: ' . $mapLng . ' }';
            ?>
            <div class="details-location <?php echo $contactColumnClass; ?>">
              <div class="inner">
                <gmap-map
                  :center="<?php echo $mapLocation; ?>"
                  :zoom="<?php echo intval($contactLocation['map_zoom']); ?>"
                  :options="elements.map.options">
                  <gmap-marker
                    :position="<?php echo $mapLocation; ?>"
                    :clickable="false"
                    :draggable="false"
                    :icon="'<?php echo $mapIcon; ?>'">
                  </gmap-marker>
                </gmap-map>
                <div class="location-body">
                  <?php echo $contactLocation['body']; ?>
                </div>
              </div>
            </div>
          <?php } ?>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<?php if ($contactFormPardotURL && !empty($contactFormPardotURL)) { ?>
  <?php
    echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/call-to-action-pardot-form', [
      'type' => 'contact',
      'url' => $contactFormPardotURL,
      'style' => 'gray',
    ]);
  ?>
<?php } ?>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/index'); ?>
<?php get_footer(); ?>
