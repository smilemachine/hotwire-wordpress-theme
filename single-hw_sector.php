<?php
if (!defined('ABSPATH')) exit;

/**
 * The main template file for a Sector
 *
 * @package Hotwire
 */

$headerTitle = get_the_title();
$headerSubtitle = get_field('company');
$headerType = Hotwire_ACF_Page_Header::TYPE_MEDIUM_STANDARD;

$quote = get_field('quote');
$quotee = get_field('quotee');
$quoteeTitle = get_field('quotee_title');
$quoteImageData = get_field('quote_image');
$quoteImageUrl = '';

if (is_array($quoteImageData) && !empty($quoteImageData)) {
  $quoteImageUrl = $quoteImageData['url'];
}

$caseStudies = Hotwire_Sector::getCaseStudies(get_the_ID(), true);
$caseStudyIDs = array_keys($caseStudies);

$titleWhatWeDo = get_field('what_we_do_title');
$titleCaseStudies = get_field('case_studies_title');
$titleThoughtLeadership = get_field('thought_leadership_title');
$thoughtLeadershipFilter = get_field('thought_leadership_filter');

if (!$titleWhatWeDo || empty($titleWhatWeDo)) {
  $titleWhatWeDo = 'What we do in the sector';
}

if (!$titleCaseStudies || empty($titleCaseStudies)) {
  $titleCaseStudies = 'Case Studies.';
}

?>
<?php
  get_header();

  echo Hotwire_Helper::getTemplatePart('template-parts/page-header', [
    'type' => $headerType,
    'title' => $headerTitle,
    'subtitle' => $headerSubtitle,
    'showGradient' => true,
  ]);
?>
<section class="overview style-light">
  <div class="container">
    <header>
      <h3><?php echo $titleWhatWeDo; ?></h3>
    </header>
    <?php the_content(); ?>
  </div>
</section>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/showcase-case-studies', [
    'title' => $titleCaseStudies,
    'itemIDs' => $caseStudyIDs
  ]);
?>
<?php if (!empty($quote)) { ?>
  <section class="quote">
    <?php if (!empty($quoteImageUrl)) { ?>
      <brands-animated-background
        :image-url="'<?php echo $quoteImageUrl; ?>'">
      </brands-animated-background>
    <?php } ?>
    <div class="container">
      <blockquote class="quote-body">
        "<?php echo $quote; ?>"
      </blockquote>
      <?php if (!empty($quotee)) { ?>
        <p class="quotee">
          <?php echo $quotee; ?>
        </p>
      <?php } ?>
      <?php if (!empty($quoteeTitle)) { ?>
        <p class="quotee-title">
          <?php echo !empty($quotee) ? ", ": null; ?><?php echo $quoteeTitle; ?>
        </p>
      <?php } ?>
    </div>
  </section>
<?php } ?>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/feed-thought-leadership', [
    'title' => $titleThoughtLeadership,
    'filter' => $thoughtLeadershipFilter,
    'readMoreText' => 'Read More',
    'limit' => 3
  ]);
?>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/index'); ?>
<?php get_footer(); ?>
