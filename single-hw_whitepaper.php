<?php
if (!defined('ABSPATH')) exit;

/**
 * The main template file
 *
 * @package Hotwire
 */

?>
<?php get_header(); ?>
<?php
	echo Hotwire_Helper::getTemplatePart('template-parts/page-header', [
		'type' => Hotwire_ACF_Page_Header::TYPE_MEDIUM_STANDARD,
		'showGradient' => true
	]);
?>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/index'); ?>
<?php get_footer(); ?>
