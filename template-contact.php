<?php
if (!defined('ABSPATH')) exit;

/**
 * The template file for the Contact page
 * Template name: Contact Template
 *
 * @package Hotwire
 */

$sessionCountryList = $_SESSION['hw_country_list'];

if (is_array($sessionCountryList) && !empty($sessionCountryList)) {
	$countryList = $sessionCountryList;
} else {
	$countryList = Hotwire_Country::get([
		'lang' => 'en',
	]);
}

$countryHeading = get_field('country_heading');

?>
<?php get_header(); ?>
<?php echo Hotwire_Helper::getTemplatePart('template-parts/page-header'); ?>
<section class="intro">
	<div class="container">
		<?php
			if (have_posts()) {
				while (have_posts()) {
					the_post();
					get_template_part('template-parts/content', get_post_format());
				}
			} else {
				get_template_part('template-parts/content', 'none');
			}
		?>
	</div>
</section>
<?php echo get_template_part('template-parts/acf-content/index'); ?>
<?php if (is_array($countryList) && !empty($countryList)) { ?>
	<section class="country-locations">
		<div class="container">
			<?php if (!empty($countryHeading)) { ?>
				<header>
					<h3><?php echo $countryHeading; ?></h3>
				</header>
			<?php } ?>
			<div class="countries">
				<div class="row">
					<?php foreach ($countryList as $country) { ?>
						<?php $countryContent = get_field('contact_body', $country->ID); ?>
						<div class="country col-xs-12 col-sm-6 col-md-4 col-lg-3 <?php echo !empty($countryContent) ? 'has-content' : null; ?>">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    <?php echo $country->post_title; ?>
							  </button>
								<?php if (!empty($countryContent)) { ?>
								  <div class="dropdown-menu">
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle">
												<?php echo $country->post_title; ?>
											</button>
										</div>
										<div class="country-content">
								    	<?php echo $countryContent; ?>
										</div>
								  </div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>
<?php get_footer(); ?>
