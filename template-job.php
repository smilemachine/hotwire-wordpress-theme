<?php
if (!defined('ABSPATH')) exit;

/**
 * The template file for the Job page
 * Template name: Job Template
 *
 * @package Hotwire
 */

$id = intval(get_query_var('page'));
$greenhouse = new Hotwire_Service_Greenhouse();
$job = $greenhouse->getJob(intval($id));

?>
<?php get_header(); ?>
<?php
  echo Hotwire_Helper::getTemplatePart('template-parts/page-header', [
    'title' => $job->title,
    'subtitle' => $job->location->name,
  ]);
?>
<div class="container">
  <article class="content">
    <?php echo html_entity_decode($job->content); ?>
    <div class="btn-container">
      <a href="<?php echo $job->absolute_url; ?>" target="_blank" class="btn btn-primary">
        Apply Now
      </a>
    </div>
  </article>
</div>
<?php get_template_part('template-parts/acf-content/index'); ?>
<?php get_footer(); ?>
