<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: call-to-action.get-started
 *
 * @package Hotwire
 */

$title = get_sub_field('get_started_title');
$button = get_sub_field('get_started_button');
$style = strtolower(get_sub_field('get_started_style'));
$url = get_sub_field('get_started_url');

if (empty($title)) {
  $title = 'Let\'s get the ball rolling.';
}

if (empty($button)) {
  $button = 'Get in touch';
}

if (empty($url)) {
  $url = get_site_url(null, '/contact');
}

?>
<section class="call-to-action get-started style-<?php echo $style; ?>">
  <div class="container">
    <header>
      <h2 class="title">
        <?php echo $title; ?>
      </h2>
    </header>
    <a href="<?php echo $url; ?>" class="btn btn-primary">
      <?php echo $button; ?>
    </a>
  </div>
</section>
