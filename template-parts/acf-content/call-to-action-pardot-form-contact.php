<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: call-to-action.pardot-form-contact
 *
 * @package Hotwire
 */

?>
<section class="call-to-action pardot-form pardot-form-contact style-<?php echo $style; ?>">
  <div class="container">
    <iframe src="<?php echo $url; ?>" type="text/html"></iframe>
  </div>
</section>
