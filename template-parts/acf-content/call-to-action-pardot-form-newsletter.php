<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: call-to-action.pardot-form-newsletter
 *
 * @package Hotwire
 */

?>
<section class="call-to-action pardot-form pardot-form-newsletter">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <iframe src="<?php echo $url; ?>" type="text/html"></iframe>
      </div>
    </div>
  </div>
</section>
