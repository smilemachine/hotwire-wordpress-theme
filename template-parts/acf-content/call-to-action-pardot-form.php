<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: call-to-action.pardot-form
 *
 * @package Hotwire
 */

if (!isset($type) || empty($type)) {
  $type = strtolower(get_sub_field('pardot_form_type'));
}

if (!isset($url) || empty($url)) {
  $url = get_sub_field('pardot_form_url');
}

if (!isset($style) || empty($style)) {
  $style = 'white';
}

$filename = 'call-to-action-pardot-form-' . $type;

echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/' . $filename, [
  'url' => $url,
  'style' => $style,
]);
