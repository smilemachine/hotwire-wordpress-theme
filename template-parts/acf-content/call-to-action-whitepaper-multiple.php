<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: call-to-action.whitepaper.multiple
 *
 * @package Hotwire
 */

?>
<?php if (isset($whitepapers) && $whitepapers && count($whitepapers) == 2) { ?>
  <?php
    if (!isset($button) || empty($button)) {
      $button = get_sub_field('whitepaper_button');
    }

    if (empty($button)) {
      $button = 'Download';
    }

    if (!isset($style)) {
      $style = 'white';
    }

    if (!isset($title) || empty($title)) {
      $title = get_sub_field('whitepaper_title');
    }

    $randomIndexes = array_rand($whitepapers, 2);
    $randomWhitepapers = [];

    foreach ($randomIndexes as $index) {
      $randomWhitepapers[] = $whitepapers[$index];
    }
  ?>
  <section class="call-to-action whitepaper style-<?php echo $style; ?> whitepaper-multiple">
    <div class="container">
      <?php if (!empty($title)) { ?>
        <header>
          <h2 class="title">
            <?php echo $title; ?>
          </h2>
        </header>
      <?php } ?>
      <div class="row">
        <?php foreach ($randomWhitepapers as $whitepaper) { ?>
          <div class="col-md-6">
            <article class="whitepaper-item">
              <a href="<?php echo get_permalink($whitepaper->ID); ?>">
                <slick-image
                  :initial="'<?php echo Hotwire_Helper::getPostThumbnailUrl($whitepaper->ID, 'medium'); ?>'"
                  :full="'<?php echo Hotwire_Helper::getPostThumbnailUrl($whitepaper->ID, 'medium'); ?>'">
                </slick-image>
              </a>
                <section class="content">
                  <header>
                    <a href="<?php echo get_permalink($whitepaper->ID); ?>">
                      <h2 class="title">
                        <?php echo $whitepaper->post_title; ?>
                      </h2>
                    </a>
                  </header>
                  <div class="body">
                    <?php echo apply_filters('the_content', get_field('blurb', $whitepaper->ID)); ?>
                  </div>
                  <div class="btn-container">
                    <a href="<?php echo get_permalink($whitepaper->ID); ?>" class="btn btn-primary">
                      <?php echo $button; ?>
                    </a>
                  </div>
                </section>
              </a>
            </article>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
<?php } ?>
