<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: call-to-action.whitepaper.single
 *
 * @package Hotwire
 */

?>
<?php if (isset($whitepaper) && $whitepaper) { ?>
  <?php
    $imageUrl = Hotwire_Helper::getPostThumbnailUrl($whitepaper->ID, 'medium');

    if (!isset($button) || empty($button)) {
      $button = get_sub_field('whitepaper_button');
    }

    if (empty($button)) {
      $button = 'Download';
    }

    if (!isset($buttonUrl) || empty($buttonUrl)) {
      $buttonUrl = get_the_permalink($whitepaper->ID);
    }

    if (!isset($style)) {
      $style = 'light';
    }

    if (!isset($title) || empty($title)) {
      $title = get_sub_field('whitepaper_title');
    }
  ?>
  <section class="call-to-action whitepaper style-<?php echo $style; ?> whitepaper-single">
    <div class="container">
      <?php if (!empty($title)) { ?>
        <header>
          <h2 class="title">
            <?php echo $title; ?>
          </h2>
        </header>
      <?php } ?>
      <header class="visible-xs visible-sm">
        <h2 class="title">
          <?php echo $whitepaper->post_title; ?>
        </h2>
      </header>
      <div class="row">
        <div class="col-md-6 col-lg-5 col-md-push-6 col-lg-push-7">
          <img src="<?php echo $imageUrl; ?>">
        </div>
        <div class="col-md-6 col-lg-5 col-md-pull-6 col-lg-pull-5">
          <header class="hidden-xs hidden-sm">
            <h2 class="title">
              <?php echo $whitepaper->post_title; ?>
            </h2>
          </header>
          <div class="body">
            <?php echo apply_filters('the_content', $whitepaper->post_content); ?>
          </div>
          <a href="<?php echo $buttonUrl; ?>" class="btn btn-primary">
            <?php echo $button; ?>
          </a>
        </div>
      </div>
    </div>
  </section>
<?php } ?>
