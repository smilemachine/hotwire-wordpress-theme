<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: call-to-action.whitepaper
 *
 * @package Hotwire
 */
$singleWhitepaper = true;

if (!isset($whitepapers) || !$whitepapers || !is_array($whitepapers)) {
 $whitepapers = get_sub_field('whitepaper');
}

if (is_array($whitepapers) && count($whitepapers) > 1) {
  $singleWhitepaper = false;
}

if (!isset($whitepaper) || !$whitepaper) {
  if (is_array($whitepapers) && !empty($whitepapers)) {
    $whitepaper = $whitepapers[0];
  }
}

?>
<?php if (isset($whitepaper) && $whitepaper) { ?>
  <?php
    if ($singleWhitepaper) {
      echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/call-to-action-whitepaper-single', [
    		'whitepaper' => $whitepaper,
    	]);
    } else {
      echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/call-to-action-whitepaper-multiple', [
    		'whitepapers' => $whitepapers,
    	]);
    }
  ?>
<?php } ?>
