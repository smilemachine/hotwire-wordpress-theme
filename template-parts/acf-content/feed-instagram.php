<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feed.instagram
 *
 * @package Hotwire
 */

$items = [];

if (!isset($username) || empty($username)) {
  $username = get_sub_field('instagram_name');
}

if (!isset($style) || empty($style)) {
  $style = strtolower(get_sub_field('style'));
}

if (!empty($username)) {
  $instagram = new Hotwire_Service_Instagram($username);
  $items = $instagram->getFeed();
}

$itemsString = Hotwire_Helper::arrayToObjectString($items);

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <section class="feed-instagram style-<?php echo $style; ?>">
    <div class="container">
      <header>
        <h2>
          <a href="https://www.instagram.com/<?php echo $username; ?>" class="fa fa-instagram" target="_blank">
          </a>
        </h2>
      </header>
    </div>
    <instagram-feed
      :items="<?php echo $itemsString; ?>">
    </instagram-feed>
  </section>
<?php } ?>
