<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feed.jobs-global
 *
 * @package Hotwire
 */

$greenhouse = new Hotwire_Service_Greenhouse();
$items = $greenhouse->getFeed();

?>
<section class="feed-jobs style-<?php echo $style; ?>">
  <div class="container">
    <header>
      <h2>
        <?php echo $title; ?>
      </h2>
    </header>
    <?php if (!is_object($items) || !$items) { ?>
      <p class="empty-global">
        <?php echo $noPositionsGlobal; ?>
      </p>
    <?php } else { ?>
      <?php foreach ($items as $key => $region) { ?>
        <?php if (isset($region->title)) { ?>
          <section class="region">
            <header>
              <h3><?php echo $region->title; ?></h3>
            </header>
            <div class="jobs">
              <?php if (isset($region->jobs) && is_array($region->jobs) && !empty($region->jobs)) { ?>
                <ul>
                  <?php foreach ($region->jobs as $job) { ?>
                    <?php if (isset($job->url) && isset($job->title)) { ?>
                      <li>
                        <a href="<?php echo $job->url; ?>">
                          <span class="job"><?php echo $job->title; ?></span>
                          <span class="readmore">
                            <?php echo $readMore; ?>
                            <span class="caret"></span>
                          </span>
                        </a>
                      </li>
                    <?php } ?>
                  <?php } ?>
                </ul>
              <?php } else { ?>
                <p class="empty-region">
                  <?php echo $noPositionsRegion; ?>
                </p>
              <?php } ?>
            </div>
          </section>
        <?php } ?>
      <?php } ?>
    <?php } ?>
  </div>
</section>
