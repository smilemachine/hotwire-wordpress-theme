<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feed.jobs-username
 *
 * @package Hotwire
 */

$greenhouse = new Hotwire_Service_Greenhouse();
$items = $greenhouse->getFeedForUsernames($usernames);

?>
<section class="feed-jobs style-<?php echo $style; ?>">
  <div class="container">
    <header>
      <h2>
        <?php echo $title; ?>
      </h2>
    </header>
    <section class="region">
      <div class="jobs">
        <?php if (is_array($items) && !empty($items)) { ?>
          <ul>
            <?php foreach ($items as $job) { ?>
              <?php if (isset($job->url) && isset($job->title)) { ?>
                <li>
                  <a href="<?php echo $job->url; ?>">
                    <span class="job"><?php echo $job->title; ?></span>
                    <span class="readmore">
                      <?php echo $readMore; ?>
                      <span class="caret"></span>
                    </span>
                  </a>
                </li>
              <?php } ?>
            <?php } ?>
          </ul>
        <?php } else { ?>
          <p class="empty-region">
            <?php echo $noPositionsRegion; ?>
          </p>
        <?php } ?>
      </div>
    </section>
  </div>
</section>
