<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feed.jobs
 *
 * @package Hotwire
 */

$items = [];

if (!isset($style) || empty($style)) {
  $style = strtolower(get_sub_field('style'));
}

if (!isset($title) || empty($title)) {
  $title = get_sub_field('greenhouse_title');
}

if (!isset($noPositionsGlobal) || empty($noPositionsGlobal)) {
  $noPositionsGlobal = get_sub_field('greenhouse_no_positions_global');
}

if (!isset($noPositionsRegion) || empty($noPositionsRegion)) {
  $noPositionsRegion = get_sub_field('greenhouse_no_positions_region');
}

if (!isset($readMore) || empty($readMore)) {
  $readMore = get_sub_field('greenhouse_read_more');
}

if (empty($readMore)) {
  $readMore = 'Find out more';
}

if (isset($usernames) && is_array($usernames) && !empty($usernames)) {
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/feed-jobs-usernames', [
    'noPositionsRegion' => $noPositionsRegion,
    'readMore' => $readMore,
    'style' => $style,
    'title' => $title,
    'usernames' => $usernames,
  ]);
} else {
  echo Hotwire_Helper::getTemplatePart('template-parts/acf-content/feed-jobs-global', [
    'noPositionsGlobal' => $noPositionsGlobal,
    'noPositionsRegion' => $noPositionsRegion,
    'readMore' => $readMore,
    'style' => $style,
    'title' => $title,
  ]);
}
