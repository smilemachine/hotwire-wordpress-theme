<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feed.passle
 *
 * @package Hotwire
 */

// Title
if (!isset($title)) {
  $title = get_sub_field('thoughtleadership_title');
}

if (empty($title)) {
  $title = Hotwire_Helper::getTranslation('Thought Leadership.');
}

// Read More
if (!isset($readMoreText)) {
  $readMoreText = Hotwire_Helper::getTranslation('Read More');
}

if (empty($readMoreText)) {
  $readMoreText = Hotwire_Helper::getTranslation('Read More');
}

// Filter
if (!isset($filter)) {
  $filter = get_sub_field('thoughtleadership_filter');
}

if (empty($filter)) {
  $filter = Hotwire_Helper::getTranslation('Global');
}

// Limit
if (!isset($limit)) {
  $limit = intval(get_sub_field('thoughtleadership_limit'));

  if ($limit == 0) {
    $limit = 3;
  }
}

$loadMoreText = get_sub_field('thoughtleadership_load_more');

?>
<section class="feed-thought-leadership style-white layout-<?php echo $limit == 3 ? 'even' : 'offset'; ?>">
  <div class="container">
    <header>
      <h2><?php echo $title; ?></h2>
    </header>
    <section class="thought-leadership-container">
      <div class="inner row">
        <div id="passle-plugin-container"
          data-limit="<?php echo $limit; ?>"
          data-filter="<?php echo $filter; ?>"
          data-readmore="<?php echo $readMoreText; ?>"
          data-loadmore="<?php echo $loadMoreText; ?>">
        </div>
      </div>
    </section>
  </div>
</section>
