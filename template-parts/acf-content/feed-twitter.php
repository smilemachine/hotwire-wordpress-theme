<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feed.instagram
 *
 * @package Hotwire
 */

$items = [];

if (!isset($username) || empty($username)) {
  $username = get_sub_field('twitter_name');
}

if (!isset($style) || empty($style)) {
  $style = strtolower(get_sub_field('style'));
}

if (empty($style)) {
  $style = 'white';
}

if (!empty($username)) {
  $twitter = new Hotwire_Service_Twitter($username);
  $items = $twitter->getTweetIDs();
}

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <section class="feed-twitter style-<?php echo $style; ?>">
    <div class="container">
      <header>
        <h2>
          <a href="https://www.twitter.com/<?php echo $username; ?>" class="fa fa-twitter" target="_blank">
          </a>
        </h2>
      </header>
      <div class="inner">
        <?php foreach ($items as $item) { ?>
          <tweet
            :id="'<?php echo $item; ?>'">
          </tweet>
        <?php } ?>
      </div>
    </div>
  </section>
<?php } ?>
