<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: feed
 *
 * @package Hotwire
 */

$type = str_replace(' ', '-', strtolower(get_sub_field('type')));
$filename = 'feed-' . $type;
get_template_part('template-parts/acf-content/' . $filename, null);
