<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: image.content
 *
 * @package Hotwire
 */

$title = get_sub_field('title');
$body = get_sub_field('body');
$image = get_sub_field('image');
$imageUrl = $image['sizes']['medium'];
$style = strtolower(get_sub_field('style'));

?>
<section class="image-content style-<?php echo $style; ?>">
  <div class="container">
    <?php if (!empty($title)) { ?>
      <header>
        <h2 class="title">
          <?php echo $title; ?>
        </h2>
      </header>
    <?php } ?>
    <div class="row">
      <div class="col-md-6 col-lg-5 col-md-push-6 col-lg-push-7">
        <img src="<?php echo $imageUrl; ?>">
      </div>
      <div class="col-md-6 col-lg-5 col-md-pull-6 col-lg-pull-5">
        <div class="body">
          <?php echo $body; ?>
        </div>
      </div>
    </div>
  </div>
</section>
