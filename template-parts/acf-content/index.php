<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content
 *
 * @package Hotwire
 */

if (function_exists('have_rows') && have_rows('content')) {
  while (have_rows('content')) {
    the_row();
    $layout = str_replace('_', '-', get_row_layout());
    get_template_part('template-parts/acf-content/' . $layout);
  }
}
