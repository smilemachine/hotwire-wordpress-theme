<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: quote.standalone
 *
 * @package Hotwire
 */

$image = get_sub_field('image');
$imageUrl = $image['sizes']['medium'];
$quote = get_sub_field('quote');
$quotee = get_sub_field('quotee');
$quoteeTitle = get_sub_field('quotee_title');
$style = strtolower(get_sub_field('style'));

?>
<section class="quote-standalone style-<?php echo $style; ?>">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-3 col-md-offset-1 visual">
        <div class="inner">
          <img src="<?php echo $imageUrl; ?>" alt="<?php echo $quotee; ?>">
        </div>
      </div>
      <div class="col-sm-12 col-md-7 content">
        <blockquote class="quote">
          "<?php echo $quote; ?>"
        </blockquote>
        <p class="quotee">
          -<?php echo $quotee; ?>
        </p>
        <?php if (!empty($quoteeTitle)) { ?>
          <p class="quotee-title">
            <?php echo $quoteeTitle; ?>
          </p>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
