<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: quote.with-content
 *
 * @package Hotwire
 */

$image = get_sub_field('image');
$imageUrl = $image['sizes']['medium'];
$quote = get_sub_field('content_quote');
$quotee = get_sub_field('quotee');
$quoteeTitle = get_sub_field('quotee_title');
$style = strtolower(get_sub_field('style'));
$content = get_sub_field('content');
$contentTitle = get_sub_field('content_title');
$contentSubtitle = get_sub_field('content_subtitle');

?>
<section class="quote-with-content style-<?php echo $style; ?>">
  <div class="container">
    <header>
      <h2 class="title">
        <?php echo $contentTitle; ?>
      </h2>
    </header>
    <div class="inner row">
      <div class="content">
        <?php if (!empty($contentSubtitle)) { ?>
          <header>
            <h4><?php echo $contentSubtitle; ?></h4>
          </header>
        <?php } ?>
        <p><?php echo $content; ?></p>
      </div>
      <div class="quote-image">
        <img src="<?php echo $imageUrl; ?>" alt="<?php echo $quotee; ?>">
      </div>
      <div class="quote">
        <blockquote>
          "<?php echo $quote; ?>"
        </blockquote>
        <span class="quotee">
          <?php echo $quotee; ?>
        </span>
        <?php if (!empty($quoteeTitle)) { ?>
          <span class="quotee-title">
            <?php echo $quoteeTitle; ?>
          </span>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
