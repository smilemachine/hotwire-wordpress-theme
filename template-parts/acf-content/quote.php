<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: quote
 *
 * @package Hotwire
 */

$types = get_sub_field('type');

// Support entries without a type
if (is_array($types) && isset($types[0])) {
  $type = $types[0];
} else {
  $type = str_replace(' ', '-', strtolower($types));
}

$filename = 'quote-' . $type;
get_template_part('template-parts/acf-content/' . $filename, null);
