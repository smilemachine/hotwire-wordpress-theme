<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: showcase.awards
 *
 * @package Hotwire
 */

$items = Hotwire_Award::get();
$images = Hotwire_Award::getImageURLs($items);
$style = strtolower(get_sub_field('awards_style'));
$title = Hotwire_Helper::getSubField('awards_title', 'Award winning.');

?>
<?php if (is_array($images) && !empty($images) && count($images) == 5) { ?>
  <section class="showcase-awards style-<?php echo $style; ?>">
    <div class="container">
      <header>
        <h2 class="title">
          <?php echo $title; ?>
        </h2>
      </header>
      <div class="awards">
        <div class="inner">
          <?php foreach ($images as $image) { ?>
            <div class="award"
              :style="{ 'background-image': 'url(<?php echo $image; ?>)'}">
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>
