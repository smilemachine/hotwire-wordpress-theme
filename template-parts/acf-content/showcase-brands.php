<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: showcase.brands
 *
 * Note that the image will need to be at a ratio of 2:1
 *
 * @package Hotwire
 */

$content = new Hotwire_ACF_Showcase_Brands();

if (isset($imageUrl)) {
  $content->setImageUrl($imageUrl);
}

if (isset($itemIDs)) {
  $content->setItems($itemIDs);
}

$imageUrl = $content->getImageUrl();
$items = $content->getItems();
$itemsString = Hotwire_Helper::arrayToObjectString($items);
$readMore = Hotwire_Helper::getTranslation('Read More');

?>
<brands-carousel
  :image-url="'<?php echo $imageUrl; ?>'"
  :items="<?php echo $itemsString; ?>"
  :read-more="'<?php echo $readMore; ?>'">
</brands-carousel>
