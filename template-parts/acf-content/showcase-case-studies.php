<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: showcase.case-studies
 *
 * @package Hotwire
 */

$content = new Hotwire_ACF_Showcase_CaseStudy();

if (isset($itemIDs)) {
  $content->setItems($itemIDs);
}

if (isset($title)) {
  $title = $content->setTitle($title);
}

$title = $content->getTitle();
$itemIDs = $content->getItemIDs(true);

?>
<?php if (is_array($itemIDs) && !empty($itemIDs)) { ?>
  <section class="showcase-casestudies">
    <div class="container">
      <header>
        <h2 class="title">
          <?php echo $title; ?>
        </h2>
      </header>
      <?php echo do_shortcode('[hw_casestudies ids="' . implode(',', $itemIDs) . '"]'); ?>
    </div>
  </section>
<?php } ?>
