<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: showcase.expertise
 *
 * @package Hotwire
 */

$items = Hotwire_Config::getExpertiseSectors();
$title = get_sub_field('expertise_title');
$style = strtolower(get_sub_field('expertise_style'));

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <section class="showcase-expertise style-<?php echo $style; ?>">
    <div class="container">
      <?php if (!empty($title)) { ?>
        <header>
          <h2 class="title">
            <?php echo $title; ?>
          </h2>
        </header>
      <?php } ?>
      <div class="content">
        <div class="inner row">
          <?php foreach ($items as $item) { ?>
            <div>
              <?php if ($item['sector']) { ?>
                <a href="<?php echo get_the_permalink($item['sector']); ?>">
                  <?php echo $item['title']; ?>
                </a>
              <?php } else { ?>
                <span><?php echo $item['title']; ?></span>
              <?php } ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>
