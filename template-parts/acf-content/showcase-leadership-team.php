<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: showcase.leadership-team
 *
 * @package Hotwire
 */

$title = get_sub_field('leadershipteam_title');
$items = get_sub_field('leadershipteam_members');

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <section class="showcase-leadershipteam">
    <div class="container">
      <header>
        <h2 class="title">
          <?php echo $title; ?>
        </h2>
      </header>
      <div class="inner masonry-grid">
        <?php foreach ($items as $item) { ?>
          <?php $hasSocial = !empty($item['linkedin_url']) || !empty($item['twitter_url']); ?>
          <article class="team-member">
            <div class="visual">
              <img src="<?php echo $item['image']['sizes']['medium']; ?>" alt="<?php echo $item['title']; ?>">
              <div class="overlay">
                <div class="content <?php echo $hasSocial ? 'has-social' : null; ?>">
                  <?php echo $item['content']; ?>
                </div>
                <?php if ($hasSocial) { ?>
                  <div class="social">
                    <?php if (!empty($item['linkedin_url'])) { ?>
                      <a href="<?php echo $item['linkedin_url']; ?>" target="_blank">
                        <span class="fa fa-linkedin"></span>
                      </a>
                    <?php } ?>
                    <?php if (!empty($item['twitter_url'])) { ?>
                      <a href="<?php echo $item['twitter_url']; ?>" target="_blank">
                        <span class="fa fa-twitter"></span>
                      </a>
                    <?php } ?>
                  </div>
                <?php } ?>
              </div>
            </div>
            <p class="name">
              <?php echo $item['title']; ?>
            </p>
            <p class="position">
              <?php echo $item['position']; ?>
            </p>
          </article>
        <?php } ?>
      </div>
    </div>
  </section>
<?php } ?>
