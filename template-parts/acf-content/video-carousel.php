<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: video.carousel
 *
 * @package Hotwire
 */

$image = get_sub_field('image');
$title = get_sub_field('carousel_title');
$items = get_sub_field('carousel_items');
$url = get_sub_field('youtube_embed_url');

$itemsString = Hotwire_Helper::arrayToObjectString($items);

?>
<section class="video-carousel">
  <div class="container">
    <header>
      <h2 class="title">
        <?php echo $title; ?>
      </h2>
    </header>
    <div class="inner">
      <div class="visual-container">
        <slick-image
          :initial="'<?php echo $image['sizes']['initial']; ?>'"
          :full="'<?php echo $image['sizes']['medium']; ?>'">
          <a class="play"
            :href="'<?php echo $url; ?>'"
            @click.prevent="launchVideoFullscreen('<?php echo $url; ?>')">
          </a>
        </slick-image>
      </div>
      <div class="content-container slides-<?php echo count($items); ?>">
        <vertical-carousel
          :items="<?php echo $itemsString; ?>">
        </vertical-carousel>
      </div>
    </div>
  </div>
</section>
