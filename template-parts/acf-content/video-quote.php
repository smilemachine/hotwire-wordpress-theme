<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: video.quote
 *
 * @package Hotwire
 */

$image = get_sub_field('image');
$body = get_sub_field('quote_body');
$quotee = get_sub_field('quote_quotee');
$quoteeTitle = get_sub_field('quote_quotee_title');
$title = get_sub_field('quote_title');
$url = get_sub_field('youtube_embed_url');

?>
<section class="video-quote">
  <div class="container visible-xs visible-sm">
    <header>
      <h2 class="title">
        <?php echo $title; ?>
      </h2>
    </header>
  </div>
  <div class="inner">
    <div class="content">
      <div class="row">
        <div class="col-md-10">
          <header class="hidden-xs hidden-sm">
            <h2 class="title">
              <?php echo $title; ?>
            </h2>
          </header>
          <blockquote class="quote">
            "<?php echo $body; ?>"
          </blockquote>
          <?php if (!empty($quotee) || !empty($quoteeTitle)) { ?>
            <aside class="information">
              <?php if (!empty($quotee)) { ?>
                <span class="quotee">
                  <?php echo $quotee; ?>
                </span>
              <?php } ?>
              <?php if (!empty($quoteeTitle)) { ?>
                <span class="quotee-title">
                  <?php echo $quoteeTitle; ?>
                </span>
              <?php } ?>
            </aside>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="visual" style="background-image: url(<?php echo $image['url']; ?>)">
      <a class="play"
        :href="'<?php echo $url; ?>'"
        @click.prevent="launchVideoFullscreen('<?php echo $url; ?>')">
      </a>
    </div>
  </div>
</section>
