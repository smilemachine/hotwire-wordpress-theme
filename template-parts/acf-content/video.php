<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF flexible content: video
 *
 * @package Hotwire
 */

$type = str_replace(' ', '-', strtolower(get_sub_field('type')));
$filename = 'video-' . $type;
get_template_part('template-parts/acf-content/' . $filename, null);
