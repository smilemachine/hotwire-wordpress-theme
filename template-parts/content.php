<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hotwire
 */

ob_start();
the_content();
$content = ob_get_clean();

?>
<?php if (!empty($content)) { ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if (is_single() && get_post_type() === 'post') { ?>
			<div class="entry-meta">
				<div class="row">
					<div class="col-sm-6 author">
						By <?php echo esc_html(get_the_author()); ?>
					</div>
					<div class="col-sm-6 date">
						<?php echo date('j F, Y', get_the_time('U')); ?>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php echo $content ?>
	</article>
<?php } ?>
