<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hotwire
 */

?>
<section class="no-results not-found">
	Nothing found.
</section>
