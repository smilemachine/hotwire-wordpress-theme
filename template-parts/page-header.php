<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for ACF page header
 *
 * @package Hotwire
 */


if (!isset($imageInitialUrl) || empty($imageInitialUrl)) {
  $imageInitialUrl = Hotwire_Helper::getPostThumbnailUrl(get_the_ID(), 'initial');
}

if (!isset($imageFullUrl) || empty($imageFullUrl)) {
  $imageFullUrl = Hotwire_Helper::getPostThumbnailUrl(get_the_ID(), 'large');
}

if (!isset($type) || empty($type)) {
  $type = get_field('type');
}

if (empty($type)) {
  $type = Hotwire_ACF_Page_Header::TYPE_NO_IMAGE;
}

$typeSlug = Hotwire_Helper::getSlug($type);

if (!isset($title) || empty($title)) {
  $title = get_field('title');
}

if (empty($title)) {
  $title = get_the_title();
}

if (!isset($subtitle) || empty($subtitle)) {
  $subtitle = get_field('subtitle');
}

$gradient = isset($showGradient) && $showGradient;
// $back = isset($showBack) && $showBack;
$back = false;

?>
<?php if (!empty($title)) { ?>
  <section class="page-header page-header-<?php echo $typeSlug; ?>"
    is="slick-image"
    :initial="'<?php echo $imageInitialUrl; ?>'"
    :full="'<?php echo $imageFullUrl; ?>'"
    :gradient="<?php echo $gradient ? 'true' : 'false'; ?>">
    <div class="container" slot="default">
      <div class="inner">
        <header>
          <?php if ($back) { ?>
            <a href="<?php echo get_site_url(null, 'work'); ?>" class="back">
              <span class="fa fa-chevron-left"></span>
              <?php echo Hotwire_Helper::getTranslation('Back'); ?>
            </a>
          <?php } ?>
          <h1><?php  echo $title; ?></h1>
          <?php if ($subtitle && !empty($subtitle)) { ?>
            <h2><?php echo $subtitle; ?></h2>
          <?php } ?>
        </header>
      </div>
    </div>
  </section>
<?php } ?>
