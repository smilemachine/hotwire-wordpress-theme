<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hotwire
 */

ob_start();
the_content();
$content = ob_get_clean();

?>
<?php if (!empty($content)) { ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php echo $content ?>
	</article>
<?php } ?>
