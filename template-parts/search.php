<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hotwire
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	Search.
</article>
