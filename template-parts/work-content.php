<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for work template: content
 *
 * @package Hotwire
 */

$items = Hotwire_CaseStudy::get();

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <section class="work-content">
    <?php foreach ($items as $key => $item) { ?>
      <?php
        $company = get_field('company', $item->ID);
        $overview = get_field('campaign_overview', $item->ID);
        $countryAbbr = Hotwire_Helper::getTranslation('Global');
        $title = $item->post_title;
        $imageInitialUrl = Hotwire_Helper::getPostThumbnailUrl($item->ID, 'initial');
        $imageFullUrl = Hotwire_Helper::getPostThumbnailUrl($item->ID, 'large');

        $countries = get_field('country', $item->ID);

        if (is_array($countries) && count($countries) <= 2) {
          $countryAbbrs = [];

          foreach ($countries as $country) {
            $countryAbbrs[] = get_field('abbreviation', $country->ID);
          }

          $countryAbbr = implode('/', $countryAbbrs);
        }

        $services = [];
        $itemServices = get_the_terms($item->ID, Hotwire_Taxonomy_Service::TAXONOMY);

        if (is_array($itemServices) && !empty($itemServices)) {
          foreach ($itemServices as $service) {
            $services[] = 'filter-' . $service->slug;
          }
        }
      ?>
      <article class="work-item mix <?php echo implode(' ', $services); ?>">
        <?php
          echo Hotwire_Helper::getTemplatePart('template-parts/page-header', [
            'imageInitialUrl' => $imageFullUrl,
            'imageFullUrl' => $imageFullUrl,
            'subtitle' => $company,
            'title' => $title,
            'type' => 'Medium Standard',
            'showGradient' => true,
            'showBack' => true
          ]);
        ?>
        <a href="<?php echo get_permalink($item->ID); ?>"
          @click.prevent="launchCaseStudy(<?php echo $item->ID; ?>, $event)">
          <slick-image
            :initial="'<?php echo $imageInitialUrl; ?>'"
            :full="'<?php echo $imageFullUrl; ?>'"
            :gradient="true">
            <section class="content">
              <div class="inner">
                <div class="information">
                  <span class="country"><?php echo $countryAbbr; ?></span>
                  <span class="company"><?php echo $company; ?></span>
                </div>
                <h1 class="title">
                  <?php echo $title; ?>
                </h1>
                <div class="body">
                  <?php echo $overview; ?>
                </div>
                <span class="btn btn-primary">
                  <?php echo Hotwire_Helper::getTranslation('Read More'); ?>
                </span>
              </div>
            </section>
          </slick-image>
        </a>
      </article>
    <?php } ?>
  </section>
<?php } ?>
