<?php
if (!defined('ABSPATH')) exit;

/**
 * Template part for work template: filter
 *
 * @package Hotwire
 */

$terms = get_terms('hw_taxonomy_service');
$filterAll = [];
$filters = [];

if (is_array($terms) && !empty($terms)) {
  foreach ($terms as $term) {
    if ($term->slug == 'services-snapshot') {
      $filterAll[] = $term;
    } else {
      $filters[] = $term;
    }
  }
}

$items = array_merge($filterAll, $filters);

?>
<?php if (is_array($items) && !empty($items)) { ?>
  <section class="work-filter style-white">
  	<div class="container">
      <?php if (isset($title) && !empty($title)) { ?>
    		<header>
    			<h2 class="title">
    				<?php echo $title; ?>
    			</h2>
    		</header>
      <?php } ?>
  		<?php if (is_array($items) && !empty($items)) { ?>
  			<div class="row filters" data-first=".filter-<?php echo $items[0]->slug; ?>">
  				<?php foreach ($items as $item) { ?>
  					<a class="filter col-xs-12 col-sm-6 col-md-4" data-filter=".filter-<?php echo $item->slug; ?>">
  						<?php echo $item->name; ?>
  					</a>
  				<?php } ?>
  			</div>
  		<?php } ?>
  	</div>
  </section>
<?php } ?>
