<?php
if (!defined('ABSPATH')) exit;

/**
 * The template file for the Work page
 * Template name: Work Template
 *
 * @package Hotwire
 */

$caseStudies = Hotwire_CaseStudy::get();

?>
<?php get_header(); ?>
<?php
	echo Hotwire_Helper::getTemplatePart('template-parts/page-header', [
		'showGradient' => true
	]);
	echo Hotwire_Helper::getTemplatePart('template-parts/work-filter', [
		'title' => get_the_title()
	]);
	echo Hotwire_Helper::getTemplatePart('template-parts/work-content');
?>
<?php get_template_part('template-parts/acf-content/index'); ?>
<?php get_footer(); ?>
